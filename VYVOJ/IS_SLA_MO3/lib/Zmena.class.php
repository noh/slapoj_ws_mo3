<?php

class Zmena {

   public function ImportZmena($CisloSmlouvy, $PoradiVozidla, $TypAkce, $DatumZmeny, $PojisteniDo, $KodUkonceniDuvod, $KodUkonceniNarok, $SPZ, $CisloTP) {
     $Vysledek = new OutNS;
     $mdb_conn=odbc_connect(Conf::db_name, Conf::db_user, Conf::db_passw);
     odbc_setoption($mdb_conn,1,SQL_ATTR_COMMIT,SQL_TXN_NO_COMMIT);
     //Zjištění posledního ID
     $mdb_com = "select max(RWSZID) from " . Conf::db_RWSZ;
	   $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
	   if (odbc_fetch_row($mdb_rslt)) {
	     $IDx = odbc_result($mdb_rslt, 1);
	     if ($IDx == '') {
	       $IDx = 1;
			 } else {
	       $IDx++;
	     }
	   } else {
		   $Vysledek = $this->AddError($Vysledek, "RWSZID", "Interní chyba, nelze načíst ID!");
		   return $Vysledek;
		 }
     
     if (($CisloTP != "") && (!preg_match('/^[A-Z]{2}[0-9]{6}$/', $CisloTP)) && ($CisloTP != "FAKTURA") && ($CisloTP != "DOVOZ")) {
		   $Vysledek = $this->AddError($Vysledek, "CisloTP", "Špatně zadané číslo technického průkazu!");
		   return $Vysledek;
		 } 
     
     
     $mdb_com = "INSERT INTO " . Conf::db_RWSZ . " (RWSZID, QDESIN, QVEHID, RWSZTY, RWSZDT, RWSZDO, RWSZKD, RWSZKN, RWSZRZ, RWSZTP) VALUES($IDx, $CisloSmlouvy, $PoradiVozidla, '$TypAkce', $DatumZmeny, $PojisteniDo, '$KodUkonceniDuvod', '$KodUkonceniNarok', '$SPZ', '$CisloTP')";
     $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
     
     if (!$mdb_rslt) {
		    $Vysledek = $this->AddError($Vysledek, "RWSZ", "Nelze předat požadavek - pravděpodobně chybná data!");
		    return $Vysledek;
		 }
     
     //Volání LANSA kontrolní fce
     $conn = i5_connect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
     if (!$conn) {
        $Vysledek = $this->AddError($Vysledek, "RWSSCHG", "Interní chyba - connect!");
        return $Vysledek;
     }
     $cmd = i5_command("LANSA REQUEST(RUN) PROCESS(RWSSPR01) FUNCTION(RWSSCHG) PARTITION('VES') LANGUAGE('CZE ')");
     if (!$cmd) {
       $Vysledek = $this->AddError($Vysledek, "RWSSCHG", "Interní chyba - Volání RWSSCHG!");
       return $Vysledek;
     }
  
     //Přečtení výsledku
     $mdb_com = "select * from " . Conf::db_RWSZ . " where RWSZID = $IDx";
	   $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
	   if (odbc_fetch_row($mdb_rslt)) {
	     $Status = odbc_result($mdb_rslt, "RWSZST");
	     $Vysledek->Status = $Status;
			 if ($Status == 'ER') {
			    $Vysledek = $this->AddError($Vysledek, "OutNS", iconv("ISO8859-2", "UTF-8", trim(odbc_result($mdb_rslt, "RWSZMS"))));
			 } 
	   } else {
		   $Vysledek = $this->AddError($Vysledek, "RWSZ", "Interní chyba, nelze načíst odpověď dle ID!");
		 }
     return $Vysledek;
   }

  public function UschovaPS($CisloSmlouvy, $TypAkce, $DatumZmeny) {
  
     $Vysledek = new OutNS;
     $mdb_conn=odbc_connect(Conf::db_name, Conf::db_user, Conf::db_passw);
     odbc_setoption($mdb_conn,1,SQL_ATTR_COMMIT,SQL_TXN_NO_COMMIT);
     //Zjištění posledního ID
     $mdb_com = "select max(RWSZID) from " . Conf::db_RWSZ;
	   $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
	   if (odbc_fetch_row($mdb_rslt)) {
	     $IDx = odbc_result($mdb_rslt, 1);
	     if ($IDx == '') {
	       $IDx = 1;
			 } else {
	       $IDx++;
	     }
	   } else {
		   $Vysledek = $this->AddError($Vysledek, "RWSZID", "Interní chyba, nelze načíst ID!");
		   return $Vysledek;
		 }
     
     
     $mdb_com = "INSERT INTO " . Conf::db_RWSZ . " (RWSZID, QDESIN, QVEHID, RWSZTY, RWSZDT, RWSZDO, RWSZKD, RWSZKN, RWSZRZ, RWSZTP) VALUES($IDx, $CisloSmlouvy, 0, '$TypAkce', $DatumZmeny, 0, '', '', '', '')";
     $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
     
     if (!$mdb_rslt) {
		    $Vysledek = $this->AddError($Vysledek, "RWSZ", "Nelze předat požadavek - pravděpodobně chybná data!");
        file_put_contents("log.txt", date("H:i:s.u"). $mdb_com ." \n", FILE_APPEND);
		    return $Vysledek;
		 }
     
     //Volání LANSA kontrolní fce
     $conn = i5_connect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
     if (!$conn) {
        $Vysledek = $this->AddError($Vysledek, "RWSSCHG", "Interní chyba - connect!");
        return $Vysledek;
     }
     $cmd = i5_command("LANSA REQUEST(RUN) PROCESS(RWSSPR01) FUNCTION(RWSSCHG) PARTITION('VES') LANGUAGE('CZE ')");
     if (!$cmd) {
       $Vysledek = $this->AddError($Vysledek, "RWSSCHG", "Interní chyba - Volání RWSSCHG!");
       return $Vysledek;
     }
  
     //Přečtení výsledku
     $mdb_com = "select * from " . Conf::db_RWSZ . " where RWSZID = $IDx";
	   $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
	   if (odbc_fetch_row($mdb_rslt)) {
	     $Status = odbc_result($mdb_rslt, "RWSZST");
	     $Vysledek->Status = $Status;
			 if ($Status == 'ER') {
			    $Vysledek = $this->AddError($Vysledek, "OutNS", iconv("ISO8859-2", "UTF-8", trim(odbc_result($mdb_rslt, "RWSZMS"))));
			 } 
	   } else {
		   $Vysledek = $this->AddError($Vysledek, "RWSZ", "Interní chyba, nelze načíst odpověď dle ID!");
		 }
     return $Vysledek;
  
  
    }

  public function ObnovaPS() {
    $conn = i5_connect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
     if (!$conn) {
        $Vysledek = $this->AddError($Vysledek, "ObnovaPS", "Interní chyba - connect!");
        return $Vysledek;
     }
    $cmd = i5_command("LANSA REQUEST(RUN) PROCESS(RWSSPR02) FUNCTION(RWSSOBS) PARTITION('VES') LANGUAGE('CZE ')");
  
    }

  public function ZmenaPSOK() {
    $conn = i5_connect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
     if (!$conn) {
        $Vysledek = $this->AddError($Vysledek, "ZmenaPSOK", "Interní chyba - connect!");
        return $Vysledek;
     }
    $cmd = i5_command("LANSA REQUEST(RUN) PROCESS(RWSSPR02) FUNCTION(RWSSZOK) PARTITION('VES') LANGUAGE('CZE ')");
  
    }


	private function AddError ($Vysledek, $Nazev, $Popis) {
	    $idx = sizeof($Vysledek->Chyby);
      $Vysledek->Chyby[] = new Chyby;
      $Vysledek->Chyby[$idx]->PopisChyby = $Popis; 
      $Vysledek->Chyby[$idx]->ChybnaPolozka = $Nazev;
      $Vysledek->Status = "ER";
      return $Vysledek;
	}

}
?>