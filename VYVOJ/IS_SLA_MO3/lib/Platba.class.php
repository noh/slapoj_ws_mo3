<?php

class Platba {


  public function ImportPlatby (Array $PlatbaIn){


  try {

    $mdb_conn=odbc_connect(Conf::db_name, Conf::db_user, Conf::db_passw);
    odbc_setoption($mdb_conn,1,SQL_ATTR_COMMIT,SQL_TXN_NO_COMMIT);
    
    $mdb_com = "select max(RWSPLI) from " . Conf::db_RWSPL;
	  $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
    if (odbc_fetch_row($mdb_rslt)) {
	     $RWSPLI = odbc_result($mdb_rslt, 1);
	     if ($RWSPLI == '') {
	       $RWSPLI = 1;
	     } else {
	       $RWSPLI++;
	     }
	  } else {
	  	   return $this->SetError("Nepodařilo se přiřadit identifikátor zpracování!");
	  }
	  
	  $mdb_com = "INSERT INTO " . Conf::db_RWSPL . " (RWSPLI, RWSPLV, RWSPLC, RWSPLS) VALUES($RWSPLI, 0, 0, ' ')";
	  $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
	  if (!$mdb_rslt) 
	     return $this->SetError("Chyba při zápisu dávky do logu!");
     
    for ($pid=0;$pid<sizeof($PlatbaIn);$pid++) {
	    $mdb_com = "INSERT INTO " . Conf::db_RWSP . " (RWSPLI, QDESIN, RWSPLP, RWSPPR, RWSPSU) VALUES($RWSPLI, ".$PlatbaIn[$pid]->CisloSmlouvy.', '.$PlatbaIn[$pid]->LhutniPoj.', '.$PlatbaIn[$pid]->Provize.', '.$PlatbaIn[$pid]->Castka.')';
      $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
      if (!$mdb_rslt) 
	     return $this->SetError("Chyba při zápisu věty o platbě! castka = lhutni - provize");
    }
    
    $conn = i5_connect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
    if (!$conn) {
      $Vysledek = $this->SetError("Interní chyba - connect!");
      return $Vysledek;
    }
    $cmd = i5_command("LANSA REQUEST(RUN) PROCESS(RWSSPR01) FUNCTION(RWSSPLT) PARTITION('VES') LANGUAGE('CZE ')");
    if (!$cmd) {
      $Vysledek = $this->SetError("Interní chyba - Volání RWSSPLT!");
      return $Vysledek;
    }
		
		$mdb_com = "select * from " . Conf::db_RWSPL . " where RWSPLI = $RWSPLI";
		$mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
		if (!$mdb_rslt) 
	     return $this->SetError("Nenačetl se stav dávky z logu!");
		if (!odbc_fetch_row($mdb_rslt)) 
	     return $this->SetError("Nenačetl se stav dávky z logu!");
		
		$Status = odbc_result($mdb_rslt, "RWSPLS");
		$VarSym = odbc_result($mdb_rslt, "RWSPLV");
		$Castka = odbc_result($mdb_rslt, "RWSPLC");
		
		if ($Status == "OK"){
		  $Vysledek = new PlatbaOut;
      $Vysledek->Status = "OK";
      $Vysledek->VS = $VarSym;
      $Vysledek->CelkovaCastka = $Castka;
	    return $Vysledek;
		} else {
		  $Vysledek = new PlatbaOut;
      $Vysledek->Status = "ER";
      $mdb_com = "select * from " . Conf::db_RWSPE . " where RWSPLI = $RWSPLI";
		  $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
      while(odbc_fetch_row($mdb_rslt)){
        $idx = sizeof($Vysledek->PlatbaOutChyby);
        $Vysledek->PlatbaOutChyby[] = new PlatbaOutChyby;
        $Vysledek->PlatbaOutChyby[$idx]->PopisChyby = iconv("ISO8859-2", "UTF-8", odbc_result($mdb_rslt, "RWSPET")); 
        $Vysledek->PlatbaOutChyby[$idx]->CisloSmlouvy = odbc_result($mdb_rslt, "QDESIN");
			}
		}
        
    file_put_contents('plat_log.txt',date("H:i:s")."Status ".$Vysledek->Status."\n", FILE_APPEND);
    return $Vysledek;
    
    }
    
    catch (Exception $ex)
    {
      $Vysledek = $this->SetError($ex->getMessage());
      return $Vysledek;
    }
  }


  private function SetError ($Popis) {
	    $Vysledek = new PlatbaOut;
	    $Vysledek->Status = "ER";
	    $Vysledek->PlatbaOutChyby[] = new PlatbaOutChyby;
	    $Vysledek->PlatbaOutChyby[0]->PopisChyby = $Popis;
      file_put_contents('plat_log.txt',date("H:i:s")."Chyba ".$Popis."\n", FILE_APPEND); 
      file_put_contents('plat_log.txt',date("H:i:s")."Status ".$Vysledek->Status."\n", FILE_APPEND);
      return $Vysledek;
	}






}

?>