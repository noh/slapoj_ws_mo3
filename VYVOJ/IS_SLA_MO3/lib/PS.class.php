<?php
/*
 NOH: 2015/03/12
 do RWSL a WS zaveden kód služby (již nelze jednoznačně rozlišit jen kódem produktu))
*/

class PS {

//Výsledek operace
var $Vysledek;
//Pointer do DB
var $mdb_conn;
//Struktura Smlouva
var $Smlouva;
//Pointery
var $QFAKIN;
var $KNAGE1;
var $RWSSIZ;
var $ZDSPKO;
var $QDESIN;
var $RWSLID;

function __construct(){
   //file_put_contents("log.txt", date("H:i:s.u")." InitDB start -------------------------------\n", FILE_APPEND);
   $this->InitDB();
   //file_put_contents("log.txt", date("H:i:s.u")." InitDB konec \n", FILE_APPEND);
}


public function ImportPS($Smlouva){
  set_time_limit(0);
  
  //file_put_contents("log.txt", date("H:i:s.u")." ImportPS start \n", FILE_APPEND);

  
  
  $this->Smlouva = $Smlouva;
  $this->Vysledek = new OutNS;
  
  
  //file_put_contents("log.txt", date("H:i:s.u")." ImportRTB zacatek \n", FILE_APPEND);
  
  $this->ImportRTB('I');
  
  //file_put_contents("log.txt", date("H:i:s.u")." ImportRTB konec \n", FILE_APPEND);
  /*
  $conn = i5_pconnect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
	i5_command(Conf::i5_dlc); 
	
  */	
  
  /* to do dealoc */

  $this->CallFunction(Conf::fce_dlc);	
  
  //file_put_contents("log.txt", date("H:i:s.u")." ImportPS konec -----------------------------------\n", FILE_APPEND);
  return $this->Vysledek;
  
  
}

/*
public function SimulacePS($Smlouva){
   $this->Smlouva = $Smlouva;
   $this->Vysledek = new OdpovedVypocet;
   
   $this->ImportRTB('S');
  
   return $this->Vysledek;
}
*/

private function ImportRTB ($RWSLTY) {
	//Připojení k AS/400
	
	//file_put_contents("log.txt", date("H:i:s.u")." ImportRTB zacatek vevnitr\n", FILE_APPEND);
	/*
	if (!$conn = i5_pconnect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw)){
	   $this->Save_ERR("Import Dat", "Interní chyba serveru - nezdařil se connect!");
	   return false;
	}
	while(!i5_command(Conf::i5_alc)) {
	  //NOP
	}
	TO DO alokace
	
	
	*/
	
	while (!$this->CallFunction(Conf::fce_alc)) {
	
		// když se nepovede, co pak? ošetřit !!!
	
	}
	
	
	
  //Inicializace pointerů
  $this->QFAKIN = &$this->Smlouva->Makler;
  $this->KNAGE1 = &$this->Smlouva->MaklerKod;
  $this->RWSSIZ = &$this->Smlouva->IdentZaznamu;
  $this->ZDSPKO = &$this->Smlouva->ProduktKod;
  $this->QDESIN = &$this->Smlouva->CisloSmlouvy;
  $this->RWSTWS = &$this->Smlouva->SluzbaKod;
    
  $this->QFAKIN = iconv("UTF-8", "ISO8859-2", $this->QFAKIN);
  if (!isset($this->QDESIN))
     $this->QDESIN = 0;

  //Zápis do logu
  $RWSLIDA = get_object_vars($this->QueryDB("select max(RWSLID) from ".Conf::db_RWSL." where QFAKIN = '{$this->QFAKIN}' and KNAGE1 = {$this->KNAGE1} and RWSSIZ = {$this->RWSSIZ}"));
	$this->RWSLID = $RWSLIDA['00001'];
	if ($this->RWSLID == NULL) 
	     $this->RWSLID = 1;
	else
	     $this->RWSLID++;

  if(!$this->InsertDB("INSERT INTO ".Conf::db_RWSL." (QFAKIN, KNAGE1, RWSSIZ, RWSLID, ZDSPKO, RWSTWS, QDESIN, RWSLTY, RRIDST) VALUES('{$this->QFAKIN}', {$this->KNAGE1}, {$this->RWSSIZ}, {$this->RWSLID}, '{$this->ZDSPKO}', '{$this->RWSTWS}', {$this->QDESIN}, '$RWSLTY', 0)")){
	   $this->Save_ERR('Fatální Chyba', 'Nezaložil se log operace! Operace nemůže pokračovat!');
	   return false;
	}
	
	//Pokud není simulace
  if ($RWSLTY == 'I'){
    //Test na zámek smlouvy
    if($this->QueryDB("select * from " . Conf::db_RWSK . " where QDESIN = {$this->QDESIN}")) {
		  $this->Save_ERR("CisloSmlouvy", "Smlouva je zamčená pro import!");
		  $this->InsertDB("update ".Conf::db_RWSL." set RRIDST = -1 where QFAKIN = '{$this->QFAKIN}' and KNAGE1 = {$this->KNAGE1} and RWSSIZ = {$this->RWSSIZ} and RWSLID = {$this->RWSLID}");
		  return false;
		}
    //Proveď kontrolu existence smlouvy
    $tmpres = $this->QueryDB("select * from ".Conf::db_RRID." where QDESIN = {$this->QDESIN} and RRIDPL = 'A'");
    if(isset($tmpres->RRIDST)){
	    if ($tmpres->RRIDST >= '8') { 
		     $this->Save_ERR("CisloSmlouvy", "Import neproběhl - Smlouva již byla dříve úspěšně naimportována!");
		     $this->InsertDB("update ".Conf::db_RWSL." set RRIDST = -1 where QFAKIN = '{$this->QFAKIN}' and KNAGE1 = {$this->KNAGE1} and RWSSIZ = {$this->RWSSIZ} and RWSLID = {$this->RWSLID}");
		     return false;
		  }	
	  }
	}
	
	//Vlastní zápis do přenosového souboru
	$this->ExportObject($this->Smlouva, 'Smlouva');
	
	//file_put_contents("log.txt", date("H:i:s.u")."Volani AS400 fce zacatek \n", FILE_APPEND);
	
	//Volání AS400 LANSA funkce
	/* TO DO volani
	if(!i5_command(Conf::i5_cmd)){
	   $this->Save_ERR("Import Dat", "Interní chyba serveru - Chyba volání serverové funkce!");
	   return false;
	}
	*/
	
	$this->CallFunction(Conf::fce_import);
	
	
	//file_put_contents("log.txt", date("H:i:s.u")."Volani AS400 fce konec \n", FILE_APPEND);
	
	
	//Načtení věty z logu
	$log_veta = $this->QueryDB("select * from ".Conf::db_RWSL." where QFAKIN = '{$this->QFAKIN}' and KNAGE1 = {$this->KNAGE1} and RWSSIZ = {$this->RWSSIZ} and RWSLID = {$this->RWSLID}");
	
	
	//file_put_contents("log.txt", date("H:i:s.u")."veta z logu konec \n", FILE_APPEND);
	
	//Načtení chyb z chybového souboru
	$errarr = $this->SelectDB("select * from ".Conf::db_RWSE." where QFAKIN = '{$this->QFAKIN}' and KNAGE1 = {$this->KNAGE1} and RWSSIZ = {$this->RWSSIZ} and RWSLID = {$this->RWSLID}");
	if(is_array($errarr)){
	  foreach($errarr as $errobj){
		   $this->Save_ERR(iconv("ISO8859-2", "UTF-8", $errobj->RWSSNV), iconv("ISO8859-2", "UTF-8", $errobj->RWSEDS));
		}
	}
	
	
  //file_put_contents("log.txt", date("H:i:s.u")."nacteni chyb konec \n", FILE_APPEND);	
	
	if($log_veta->RRIDST == 0){
	   $this->Save_ERR("Import Dat", "Interní chyba serveru - Import neproběhl na straně serveru!");
	   $this->InsertDB("update ".Conf::db_RWSL." set RRIDST = -1 where QFAKIN = '{$this->QFAKIN}' and KNAGE1 = {$this->KNAGE1} and RWSSIZ = {$this->RWSSIZ} and RWSLID = {$this->RWSLID}");
	   return false;
	}

  //$this->WS_Return_Value('this->Vysledek', 'OutNS');
  $this->WS_SerRet_Value('this->Vysledek', 'OutNS');	
  
  
  file_put_contents("log.txt", date("H:i:s.u")."this->Vysledek->Status ".$this->Vysledek->Status." \n", FILE_APPEND);
  
  if ($this->Vysledek->Status == 'ER')  {
  
    if($this->QueryDB("select * from " . Conf::db_RWSI . " where QDESIN = {$this->QDESIN}")) {
      // intervence se má tvářit jako OK
		  $this->Vysledek->Status = 'OK';
	  }
  
  }
  
  if (!isset($this->Vysledek->Status)) {
     if (sizeof($this->Vysledek->Chyby) == 0)
        $this->Vysledek->Status = 'OK';
     else
       if($this->QueryDB("select * from " . Conf::db_RWSI . " where QDESIN = {$this->QDESIN}")) {
		     $this->Vysledek->Status = 'OK';
	     } else
         $this->Vysledek->Status = 'ER';
  }
  
  
  //file_put_contents("log.txt", date("H:i:s.u")."nastaveni statusu konec \n", FILE_APPEND);
  
  if (($this->Vysledek->Status == 'OK') && ($this->Smlouva->KodRezimu == '0') && (isset($this->Smlouva->DokumentURL))){
	   if ($KUPDoc = fopen ($this->Smlouva->DokumentURL, "r")) {
        while(!feof($KUPDoc)) {
           $buffer = fread($KUPDoc, 2048);
           file_put_contents(Conf::KUP_tmp . $this->QDESIN . '_Pojistna_smlouva.pdf', $buffer, FILE_APPEND);
        }
        if(!$this->InsertDB("INSERT INTO " . Conf::db_RWSF . " (QDESIN, QCDABFX, KUPFNM) VALUES({$this->QDESIN}, {$this->Smlouva->DatumPocatku}, '{$this->QDESIN}_Pojistna_smlouva.pdf')")){
	         $this->Save_ERR('Chyba Dokumentu', 'Smlouva naimportována, ale chyba při stažení dokumentu!');
        } else {
					/*	TO DO KUP
				     i5_command(Conf::i5_KUP_cmd);
				   */ 
				   $this->CallFunction(Conf::fce_kup);
				}
        fclose ($KUPDoc);
     }
	}
	
	
	//file_put_contents("log.txt", date("H:i:s.u")."ImportRTB vnitrek konec\n", FILE_APPEND);

	return true;
}

private function ExportObject($E_Object, $E_Ancestor){

   // file_put_contents("log.txt", date("H:i:s.u")." Zaba start \n", FILE_APPEND);

   $tmparr = (array)($E_Object);
   foreach($tmparr as $Element => $Value){
	    if(is_object($Value)){
	       if($Element == 'Adresa')
	          $Element = $E_Ancestor;
			   $this->ExportObject($Value, $Element);
			} else {
         $cValue = iconv("UTF-8", "ISO8859-2", $this->DoubleQuote($Value));
         $sql = "INSERT INTO ".Conf::db_RWSS." (QFAKIN, KNAGE1, RWSSIZ, RWSLID, RWSSEL, RWSSNV, RWSSVL) VALUES('{$this->QFAKIN}', {$this->KNAGE1}, {$this->RWSSIZ}, {$this->RWSLID}, '$E_Ancestor', '$Element', '$cValue')";
         //$sql = "INSERT INTO ".Conf::db_RWSS." (QFAKIN, KNAGE1, RWSSIZ, RWSLID, RWSSEL, RWSSNV, RWSSVL) VALUES('{$this->QFAKIN}', {$this->KNAGE1}, {$this->RWSSIZ}, {$this->RWSLID}, '$E_Ancestor', '$Element', '".iconv("UTF-8", "ISO8859-2", $Value)."')";
         file_put_contents("sql.txt", date("H:i:s.u")."Element ".$Element." : ".$sql, FILE_APPEND);
			   if(!$this->InsertDB($sql)) {
         //if(!$this->InsertDB("INSERT INTO ".Conf::db_RWSS." (QFAKIN, KNAGE1, RWSSIZ, RWSLID, RWSSEL, RWSSNV, RWSSVL) VALUES('{$this->QFAKIN}', {$this->KNAGE1}, {$this->RWSSIZ}, {$this->RWSLID}, '$E_Ancestor', '$Element', '".iconv("UTF-8", "ISO8859-2", $Value)."')")){
				    $this->Save_ERR("Fatální chyba", "Nezdařil se záznam hodnoty elementu!");
				    $this->Save_ERR("Fatální chyba", utf8_encode(stripslashes ($sql)));
				 }
			}
	 }
	 
  //file_put_contents("log.txt", date("H:i:s.u")." Zaba konec \n", FILE_APPEND);	 
	 
}


private function Quote($Str) // Double-quoting only
    {
    $Str=str_replace('"','\"',$Str);
    return '"'.$Str.'"';
    } // Quote

private function DoubleQuote($Str) // Double-quoting only
    {
    $Str=str_replace("'","''",$Str);
    return $Str;
    } // DoubleQuote


private function WS_SerRet_Value($ClassName, $ClassType) {

  // podle $this->RWSSIZ 
  $rootClassName = $ClassName; 
  //file_put_contents("log.txt", date("H:i:s.u")." WS_SerRet_value zacatek \n", FILE_APPEND);
  $Outarr = $this->SelectDB("select * from " . Conf::db_RWSOEXP . " where RWSSIZ = {$this->RWSSIZ}");
	if(is_array($Outarr)){
	   foreach($Outarr as $Outobj){
	      $New_object = trim($Outobj->RWSTTY);
	      $ClVName = trim($Outobj->RWSTVN);
	      if ($New_object == 'N') {
			      eval ("\$".$ClassName.'->'.$ClVName." = new $ClVName;");
			      //file_put_contents("log.txt", date("H:i:s.u")." ".$ClassName." new ".$ClVName."\n", FILE_APPEND);
			      $ClassName .= '->'.$ClVName;
			      continue;
			   }
        $WS_Value_point = $ClassName.'->'.$ClVName;
        $Out_Value = trim($Outobj->RWSOVA);
        eval ("\$$WS_Value_point = '$Out_Value';");
        //file_put_contents("log.txt", date("H:i:s.u")." ".$WS_Value_point." ".$Out_Value."\n", FILE_APPEND);
    }
  }
  //file_put_contents("log.txt", date("H:i:s.u")." WS_SerRet_value konec \n", FILE_APPEND);
 }

private function WS_Return_Value($ClassName, $ClassType) {

   //file_put_contents("log.txt", date("H:i:s.u")." WS_Return_Value zacatek\n", FILE_APPEND);

   $VarTypes = array("int", "string");
   $WStype = 'MOT';
   $Outarr = $this->SelectDB("select * from " . Conf::db_RWSTDOC . " where RWSTWS = '$WStype' and RWSTST <> 'A' and RWSTCN = '$ClassType'");
	 if(is_array($Outarr)){
	    foreach($Outarr as $Outobj){
         $ClVType = trim($Outobj->RWSTVT);
         $ClVName = trim($Outobj->RWSTVN);
         $DPARCD = trim($Outobj->RWSTFD);
         //file_put_contents("log.txt", date("H:i:s.u")." DPARCD ".$DPARCD." NEW_OBJ ".$Outobj->RWSTTY."\n", FILE_APPEND);
         $New_object = trim($Outobj->RWSTTY);
         if(in_array($ClVType, $VarTypes)) {
           if ($DPARCD != '') {
            $Outval = $this->QueryDB("select * from " . Conf::db_RPAO . " where QDESIN = {$this->QDESIN} and RPARPL = 'A' and DPARCD = '$DPARCD'");
            if ($Outval) {
               $Alpha_val = trim($Outval->QOBPVA);
               $Numeric_val = trim($Outval->QOBPVN);
               if ($Alpha_val == '')
                  $Out_Value = $Numeric_val;
               else
                  $Out_Value = $Alpha_val;
               $WS_Value_point = $ClassName.'->'.$ClVName;
               eval ("\$$WS_Value_point = '$Out_Value';");
               // file_put_contents("log.txt", date("H:i:s.u")." ".$WS_Value_point." ".$Out_Value."\n", FILE_APPEND);
            } else  {
              // posraný výjimky (neshoduje se DPARCD pro různé produkty)
              $yselect = 0;
              if ($DPARCD == 'URZ') {
                $DPARCD = 'URS';
                $yselect = 1;
              }
              if ($DPARCD == 'DZP') {
                $DPARCD = 'DZS';
                $yselect = 1;
              }
              if ($yselect == 1) {
              $Outval = $this->QueryDB("select * from " . Conf::db_RPAO . " where QDESIN = {$this->QDESIN} and RPARPL = 'A' and DPARCD = '$DPARCD'");
                if ($Outval) {
                 $Alpha_val = trim($Outval->QOBPVA);
                 $Numeric_val = trim($Outval->QOBPVN);
                 if ($Alpha_val == '')
                    $Out_Value = $Numeric_val;
                 else
                   $Out_Value = $Alpha_val;
                   $WS_Value_point = $ClassName.'->'.$ClVName;
                    eval ("\$$WS_Value_point = '$Out_Value';");
                    // file_put_contents("log.txt", date("H:i:s.u")." ".$WS_Value_point." ".$Out_Value."\n", FILE_APPEND);
                }
              }
            }
           }
  			} else {
			   if ($New_object == 'N') {
			      eval ("\$".$ClassName.'->'.$ClVName." = new $ClVName;");
			      // file_put_contents("log.txt", date("H:i:s.u")." ".$ClassName." new ".$ClVName."\n", FILE_APPEND);
			   }
			   $this->WS_Return_Value($ClassName.'->'.$ClVName, $ClVType);
			   // file_put_contents("log.txt", date("H:i:s.u")." ".$ClassName." else ".$ClVName." ".$ClVType."\n", FILE_APPEND);
			}
     }
   }
   
   // file_put_contents("log.txt", date("H:i:s.u")." WS_Return_Value konec\n", FILE_APPEND);
   
   
}

private function Save_ERR($ChybnaPolozka, $PopisChyby) {
   $idx = sizeof($this->Vysledek->Chyby);
   $this->Vysledek->Chyby[] = new Chyby;
   $this->Vysledek->Chyby[$idx]->PopisChyby = $PopisChyby; 
	 $this->Vysledek->Chyby[$idx]->ChybnaPolozka = $ChybnaPolozka;
	 $this->Vysledek->Status = "ER";
   return true;
} 

//Pomocné
private function StripDiak($s){
   $prevodni_tabulka = Array('ä'=>'a', 'Ä'=>'A', 'á'=>'a', 'Á'=>'A', 'à'=>'a', 'À'=>'A', 'ã'=>'a', 'Ã'=>'A', 'â'=>'a', 'Â'=>'A', 'č'=>'c', 'Č'=>'C', 'ć'=>'c', 'Ć'=>'C', 'ď'=>'d', 'Ď'=>'D', 'ě'=>'e', 'Ě'=>'E', 'é'=>'e', 'É'=>'E', 'ë'=>'e', 'Ë'=>'E', 'è'=>'e', 'È'=>'E', 'ê'=>'e', 'Ê'=>'E', 'í'=>'i', 'Í'=>'I', 'ï'=>'i', 'Ï'=>'I', 'ì'=>'i', 'Ì'=>'I', 'î'=>'i', 'Î'=>'I', 'ľ'=>'l', 'Ľ'=>'L', 'ĺ'=>'l', 'Ĺ'=>'L', 'ń'=>'n', 'Ń'=>'N', 'ň'=>'n', 'Ň'=>'N', 'ñ'=>'n', 'Ñ'=>'N', 'ó'=>'o', 'Ó'=>'O', 'ö'=>'o', 'Ö'=>'O', 'ô'=>'o', 'Ô'=>'O', 'ò'=>'o', 'Ò'=>'O', 'õ'=>'o', 'Õ'=>'O', 'ő'=>'o', 'Ő'=>'O', 'ř'=>'r', 'Ř'=>'R', 'ŕ'=>'r', 'Ŕ'=>'R', 'š'=>'s', 'Š'=>'S', 'ś'=>'s', 'Ś'=>'S', 'ť'=>'t', 'Ť'=>'T', 'ú'=>'u', 'Ú'=>'U', 'ů'=>'u', 'Ů'=>'U', 'ü'=>'u', 'Ü'=>'U', 'ù'=>'u', 'Ù'=>'U', 'ũ'=>'u', 'Ũ'=>'U', 'û'=>'u', 'Û'=>'U', 'ý'=>'y', 'Ý'=>'Y', 'ž'=>'z', 'Ž'=>'Z', 'ź'=>'z', 'Ź'=>'Z');
   return strtr($s, $prevodni_tabulka);
}


//DB Vrstva
private function InitDB(){
  //Připojení k DB 
  if($this->mdb_conn = odbc_connect(Conf::db_name, Conf::db_user, Conf::db_passw)) {
     odbc_setoption($this->mdb_conn,1,SQL_ATTR_COMMIT,SQL_TXN_NO_COMMIT);
     return true;
  } else {
     return false;	
	}
}

private function InsertDB($SQL_comm){
   if(odbc_exec($this->mdb_conn, $SQL_comm))
      return true;
   else
      return false;
}

private function QueryDB($SQL_comm){
   if($result = odbc_exec($this->mdb_conn, $SQL_comm)){
      return odbc_fetch_object($result);
   } else {
      return false;
   }
}

private function CallFunction($CALL_comm){
   if($result = odbc_exec($this->mdb_conn, $CALL_comm)){
      return $result;
   } else {
      return false;
   }
}

private function SelectDB($SQL_comm){
   if($result = odbc_exec($this->mdb_conn, $SQL_comm)){
      $asocar = array();
      while($resobj = odbc_fetch_object($result)){
			   $asocar[] = $resobj;
			}
			return $asocar;
   } else {
      return false;
   }
}

}
?>