<?php
/** 
  * InsuranceService
  *  
  * InsuranceService je webová služba určená pro předávání informací 
  * o pojištěných smlouvách. Součástí služby je kontrola údajů 
  * na pojistné smlouvě. 	 
  */
class InsuranceService{

	/**
	  * Zkontroluje údaje na nové předávané smlouvě.
	  * Pokud jsou v pořádku, smlouvu předá systému	  
	  * @param Smlouva
	  * @return OutNS
	  */
	public function NovaPS(Smlouva $Smlouva) {
	  $PSi = new PS;
	  $Vysledek = $PSi->ImportPS($Smlouva);
	   return $Vysledek;
	}
	
	/**
	  * Slouží pro nové nahrání smlouvy, která již byla importovaná.
	  * Lze použít pouze pro smlouvy, které ještě nebyly otaxované.	  
	  * @param Smlouva
	  * @return OutNS
	  */
	public function oZmenaPS(Smlouva $Smlouva) {
	  $Zmena = new Zmena;
	  $Vysledek = $Zmena->ImportZmena($Smlouva->CisloSmlouvy, 1, 'DL', date("Ymd"), 0, '', '', '', '');
	  if($Vysledek->Status == 'ER') {
		  return $Vysledek;
		}
	  $PSi = new PS;
	  $Vysledek = $PSi->ImportPS($Smlouva);
	   return $Vysledek;
	}
  
  
  /**
	  * Slouží pro nové nahrání smlouvy, která již byla importovaná.
	  * Lze použít pouze pro smlouvy, které ještě nebyly otaxované.	  
	  * @param Smlouva
	  * @return OutNS
	  */
  public function ZmenaPS(Smlouva $Smlouva) {
  
    file_put_contents("log.txt", date("H:i:s.u")." ZmenaPS začátek \n", FILE_APPEND);
  
    $conn = i5_connect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
    if (!$conn) {
        $Vysledek = $this->AddError($Vysledek, "ZmenaPS", "Interní chyba - connect!");
        return $Vysledek;
     }
    
    while(!i5_command(Conf::i5_alczm)) {
	   //NOP
	   }
	
	  $Zmena = new Zmena;
	  $Vysledek = $Zmena->UschovaPS($Smlouva->CisloSmlouvy, 'ZP', date("Ymd"));
	  if($Vysledek->Status == 'ER') {
      $conn = i5_connect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
      i5_command(Conf::i5_dlczm);
      file_put_contents("log.txt", date("H:i:s.u")." chyba uschova \n", FILE_APPEND);
		  return $Vysledek;
		}
    
	  $PSi = new PS;
	  $Vysledek = $PSi->ImportPS($Smlouva);
	  
    if($Vysledek->Status == 'ER') {
      // při chybě dojde k obnovení původní PS
      $Zmena->ObnovaPS();
      file_put_contents("log.txt", date("H:i:s.u")." Obnova \n", FILE_APPEND);
        // pokud se povede zmena PS, je importována
        // nová smlouva a původní zůstává pod temp číslem
        $Vysledek = $this->AddError($Vysledek, "ZmenaPS", "Nepovedl se import ZmenaPS, obnovena původní verze sml.!");
      
		} else {
    
      $Zmena->ZmenaPSOK();
      
    }
    
    i5_command(Conf::i5_dlczm);
    
    return $Vysledek;
	}
  
  
		/**
	  * Stornuje dříve předanou smlouvu
	  * @param StornoIn
	  * @return OutNS
	  */
	public function StornoPS(StornoIn $StornoIn) {
	   $Zmena = new Zmena;
     $Vysledek = $Zmena->ImportZmena($StornoIn->CisloSmlouvy, $StornoIn->PoradiVozidla, 'ST', $StornoIn->DatumStorna, 0, '', '', '', '');
	   return $Vysledek;
	}
	
	  /**
	  * Ukončí smlouvu v systému k požadovanému datu
	  * @param UkonceniIn
	  * @return UkonceniOut
	  */
	public function UkonceniPS(UkonceniIn $UkonceniIn) {
	   $Ukonceni = new Ukonceni;
	   $Vysledek = $Ukonceni->ImportUkonceni($UkonceniIn->CisloSmlouvy, $UkonceniIn->PoradiVozidla, $UkonceniIn->DatumUkonceni, $UkonceniIn->PojisteniDo, $UkonceniIn->KodUkonceniDuvod, $UkonceniIn->KodUkonceniNarok, $UkonceniIn->VracenaZK);
	   return $Vysledek;
	}
	
	/**
	  * Změní vybrané (SPZ, Čislo TP) položky na Smlouvě
	  * @param ZmenaPolIn
	  * @return OutNS
	  */
	public function ZmenaPolPS(ZmenaPolIn $ZmenaPolIn) {
     $Zmena = new Zmena;
     $Vysledek = $Zmena->ImportZmena($ZmenaPolIn->CisloSmlouvy, $ZmenaPolIn->PoradiVozidla, 'ZM', $ZmenaPolIn->DatumZmeny, 0, '', '', $ZmenaPolIn->SPZ, $ZmenaPolIn->CisloTP);
	   return $Vysledek;
	}
	
	/**
	  * Ověří předaný seznam plateb vůči systému, vrátí VS a sumu platby
	  * @param PlatbaIn[]
	  * @return PlatbaOut
	  */
	public function PlatbaPS(Array $PlatbaIn) {
     $Platba = new Platba;
     $Vysledek = $Platba->ImportPlatby($PlatbaIn);
     return $Vysledek;
	}
	
	/**
	  * Kontrola adresy vůči UIR, vrací seznam odpovídajících adres
	  * @param Adresa[]
	  * @return OdpovedAdresa[]
	  */
	public function AdresaUIR(Array $Adresa)	{
	   clearstatcache();
	   
	   $Pfile = fopen("inout/AdresyDotaz.txt", "w");
	   for($ai=0;$ai<sizeof($Adresa);$ai++) {
	     $Adresa[$ai]->PSC = str_replace(" ", "", $Adresa[$ai]->PSC);
	     fwrite($Pfile, $ai.";".mb_strtoupper($Adresa[$ai]->Ulice, "UTF-8").";".$Adresa[$ai]->CisloDomu.";".mb_strtoupper($Adresa[$ai]->Obec, "UTF-8").";".$Adresa[$ai]->PSC.";\n");
	   }
		 fclose($Pfile);
		 
		 $conn = i5_connect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
     if (!$conn) {
       $Vysledek[] = new OdpovedAdresa;
		   $Vysledek[0]->Status = "ER";
		   return $Vysledek;
     }
     $cmd = i5_command("LANSA REQUEST(RUN) PROCESS(RWSSPR01) FUNCTION(RWSSAD4) PARTITION('VES') LANGUAGE('CZE ')");
     if (!$cmd) {
       $Vysledek[] = new OdpovedAdresa;
		   $Vysledek[0]->Status = "ER";
		   return $Vysledek;
     }
     
     $PfileN = "inout/AdresyOdpoved.txt";
     $Pfile = fopen($PfileN, "r");
     if (!$Pfile) {
		   $Vysledek[] = new OdpovedAdresa;
		   $Vysledek[0]->Status = "ER";
		   return $Vysledek;
		 }
		 
		 $AdrData = fread($Pfile, filesize($PfileN));
		 $AdrData = explode("\n", $AdrData);
		 $idxp = -1;
		 $idxa = 0;
		 for($ai=0;$ai<sizeof($AdrData);$ai++) {
		    if ($AdrData[$ai] == "") {
		      continue;
				}
		    $AdrRow = explode(";", $AdrData[$ai]);
		    if ($idxp != $AdrRow[0]) {
		      $Vysledek[] = new OdpovedAdresa;
		      $idxp = $AdrRow[0];
		      $Vysledek[$AdrRow[0]]->Status = $AdrRow[1];
		      $idxa = 0;
				}
					if (($AdrRow[1] == "OK") || ($AdrRow[1] == "OU") || ($AdrRow[1] == "OP") || ($AdrRow[1] == "OV") || ($AdrRow[1] == "OC")){
				  $Vysledek[$AdrRow[0]]->Adresa[] = new Adresa;
				  $Vysledek[$AdrRow[0]]->Adresa[$idxa]->UIRid = $AdrRow[2];
				  $Vysledek[$AdrRow[0]]->Adresa[$idxa]->Ulice = $AdrRow[3];
				  $Vysledek[$AdrRow[0]]->Adresa[$idxa]->CisloDomu = $AdrRow[4];
				  $Vysledek[$AdrRow[0]]->Adresa[$idxa]->CastObce = $AdrRow[5];
				  $Vysledek[$AdrRow[0]]->Adresa[$idxa]->MestskaCast = $AdrRow[6];
				  $Vysledek[$AdrRow[0]]->Adresa[$idxa]->Obec = $AdrRow[7];
				  $Vysledek[$AdrRow[0]]->Adresa[$idxa]->PSC = $AdrRow[8];
				  //$Vysledek[$AdrRow[0]]->Adresa[$idxa]->Stat = "";
				  $idxa++;
				}
		 }
		 
		 fclose($Pfile);
		 unlink($PfileN);
		 
	   return $Vysledek;
	}										

	/**
	  * Testovací funkce služby
	  * @return string
	  */
	public function Test() {
	   return "OK";
	}	

  private function AddError ($Vysledek, $Nazev, $Popis) {
	    $idx = sizeof($Vysledek->Chyby);
      $Vysledek->Chyby[] = new Chyby;
      $Vysledek->Chyby[$idx]->PopisChyby = $Popis; 
      $Vysledek->Chyby[$idx]->ChybnaPolozka = $Nazev;
      $Vysledek->Status = "ER";
      return $Vysledek;
	}

}
?>