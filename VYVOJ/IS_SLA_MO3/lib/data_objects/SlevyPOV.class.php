<?php

/**
  * SlevyPOV
  *
  * SlevyPOV
  *
  */
class SlevyPOV {
   /** @var int */
   public $SlevaPOV;

   /** @var string */
   public $SlevaS1;

   /** @var int */
   public $SlevaS1Proc;

   /** @var string */
   public $SlevaS2;

   /** @var int */
   public $SlevaS2Proc;

   /** @var string */
   public $SlevaS3;

   /** @var int */
   public $SlevaS3Proc;

   /** @var string */
   public $SlevaS4;

   /** @var int */
   public $SlevaS4Proc;

}
?>