<?php

/**
  * Smlouva
  *
  * Smlouva
  *
  */
class Smlouva {
   /** @var string */
   public $CisloRamcoveSmlouvy;

   /** @var string */
   public $CisloSmlouvy;

   /** @var string */
   public $DatumKoncePojisteni;

   /** @var string */
   public $DatumPocatku;

   /** @var string */
   public $DatumUzavreni;

   /** @var string */
   public $DokumentURL;

   /** @var HAV */
   public $HAV;

   /** @var string */
   public $HodinaPocatku;

   /** @var int */
   public $IdentZaznamu;

   /** @var int */
   public $KodRezimu;

   /** @var Adresa */
   public $KorespondencniAdresa;

   /** @var string */
   public $Makler;

   /** @var int */
   public $MaklerKod;

   /** @var string */
   public $NahrazujeSmlouvu;

   /** @var string */
   public $ObchodniSleva;

   /** @var int */
   public $PocetSplatek;

   /** @var Subjekt */
   public $Pojisteny;

   /** @var Subjekt */
   public $Pojistnik;

   /** @var POV */
   public $POV;

   /** @var Pripojisteni */
   public $Pripojisteni;

   /** @var int */
   public $PrirazkaSplatnost;

   /** @var string */
   public $ProduktKod;

   /** @var Subjekt */
   public $Provozovatel;

   /** @var string */
   public $SipoSpojeni;

   /** @var int */
   public $SlevaPropojistenost;

   /** @var int */
   public $UpraveneRocniPojistne;

   /** @var Subjekt */
   public $Vlastnik;

   /** @var VozidloSmlouvy */
   public $VozidloSmlouvy;

   /** @var ZelenaKarta */
   public $ZelenaKarta;

   /** @var string */
   public $ZpusobPlatby;

}
?>