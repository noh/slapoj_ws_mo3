<?php

/**
  * Adresa
  *
  * Adresa
  *
  */
class Adresa {
   /** @var string */
   public $CastObce;

   /** @var string */
   public $CisloDomu;

   /** @var string */
   public $MestskaCast;

   /** @var string */
   public $Obec;

   /** @var string */
   public $PSC;

   /** @var string */
   public $Stat;

   /** @var int */
   public $UIRid;

   /** @var string */
   public $Ulice;

}
?>