<?php

/**
  * Pojistne
  *
  * Pojistne
  *
  */
class Pojistne {
   /** @var int */
   public $LhutniPojistne;

   /** @var int */
   public $PojistneAsistence;

   /** @var int */
   public $PojistneCelniSklo;

   /** @var int */
   public $PojistneHAV;

   /** @var int */
   public $PojistneOdpovednost;

   /** @var int */
   public $PojistneOkna;

   /** @var int */
   public $PojistnePOV;

   /** @var int */
   public $PojistneUrazOsob;

   /** @var int */
   public $PojistneUrazRidice;

   /** @var int */
   public $PojistneVybava;

   /** @var int */
   public $PojistneZavazadla;

   /** @var int */
   public $PojistneZivel;

   /** @var int */
   public $PredbonusemPOV;

   /** @var int */
   public $PredslevamiPOV;

   /** @var int */
   public $RocniPojistne;

   /** @var int */
   public $StretZverPojistne;

   /** @var float */
   public $VyslKoefPOV;

   /** @var int */
   public $ZakladnisazbaPOV;

}
?>