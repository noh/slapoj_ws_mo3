$Smlouva->Makler = win2utf($dat_veta[0]);
$Smlouva->MaklerKod = win2utf($dat_veta[1]);
$Smlouva->ProduktKod = win2utf($dat_veta[2]);
$Smlouva->CisloSmlouvy = win2utf($dat_veta[3]);
$Smlouva->NahrazujeSmlouvu = win2utf($dat_veta[4]);
$Smlouva->CisloRamcoveSmlouvy = win2utf($dat_veta[5]);
$Smlouva->ZpusobPlatby = win2utf($dat_veta[6]);
$Smlouva->SipoSpojeni = win2utf($dat_veta[7]);
$Smlouva->PocetSplatek = win2utf($dat_veta[8]);
$Smlouva->DatumPocatku = win2utf($dat_veta[9]);
$Smlouva->HodinaPocatku = win2utf($dat_veta[10]);
$Smlouva->DatumKoncePojisteni = win2utf($dat_veta[11]);
$Smlouva->DatumUzavreni = win2utf($dat_veta[12]);
$Smlouva->UpraveneRocniPojistne = win2utf($dat_veta[13]);
$Smlouva->ObchodniSleva = win2utf($dat_veta[14]);
$Smlouva->PrirazkaSplatnost = win2utf($dat_veta[15]);
$Smlouva->SlevaPropojistenost = win2utf($dat_veta[16]);
$Smlouva->Pojistnik = new Subjekt;
$Smlouva->Pojistnik->TitulPred = win2utf($dat_veta[17]);
$Smlouva->Pojistnik->Jmeno = win2utf($dat_veta[18]);
$Smlouva->Pojistnik->Prijmeni = win2utf($dat_veta[19]);
$Smlouva->Pojistnik->RC = win2utf($dat_veta[20]);
$Smlouva->Pojistnik->IC = win2utf($dat_veta[21]);
$Smlouva->Pojistnik->NazevFirmy = win2utf($dat_veta[22]);
$Smlouva->Pojistnik->DatumNarozeni = win2utf($dat_veta[23]);
$Smlouva->Pojistnik->StatPrislusnost = win2utf($dat_veta[24]);
$Smlouva->Pojistnik->Telefon = win2utf($dat_veta[25]);
$Smlouva->Pojistnik->Email = win2utf($dat_veta[26]);
$Smlouva->Pojistnik->TypSubjektu = win2utf($dat_veta[27]);
$Smlouva->Pojistnik->Adresa = new Adresa;
$Smlouva->Pojistnik->Adresa->Ulice = win2utf($dat_veta[28]);
$Smlouva->Pojistnik->Adresa->CisloDomu = win2utf($dat_veta[29]);
$Smlouva->Pojistnik->Adresa->MestskaCast = win2utf($dat_veta[30]);
$Smlouva->Pojistnik->Adresa->CastObce = win2utf($dat_veta[31]);
$Smlouva->Pojistnik->Adresa->Obec = win2utf($dat_veta[32]);
$Smlouva->Pojistnik->Adresa->PSC = win2utf($dat_veta[33]);
$Smlouva->Pojistnik->Adresa->Stat = win2utf($dat_veta[34]);
$Smlouva->Pojistnik->Adresa->UIRid = win2utf($dat_veta[35]);
$Smlouva->Provozovatel = new Subjekt;
$Smlouva->Provozovatel->TitulPred = win2utf($dat_veta[36]);
$Smlouva->Provozovatel->Jmeno = win2utf($dat_veta[37]);
$Smlouva->Provozovatel->Prijmeni = win2utf($dat_veta[38]);
$Smlouva->Provozovatel->RC = win2utf($dat_veta[39]);
$Smlouva->Provozovatel->IC = win2utf($dat_veta[40]);
$Smlouva->Provozovatel->NazevFirmy = win2utf($dat_veta[41]);
$Smlouva->Provozovatel->DatumNarozeni = win2utf($dat_veta[42]);
$Smlouva->Provozovatel->StatPrislusnost = win2utf($dat_veta[43]);
$Smlouva->Provozovatel->Telefon = win2utf($dat_veta[44]);
$Smlouva->Provozovatel->Email = win2utf($dat_veta[45]);
$Smlouva->Provozovatel->TypSubjektu = win2utf($dat_veta[46]);
$Smlouva->Provozovatel->Adresa = new Adresa;
$Smlouva->Provozovatel->Adresa->Ulice = win2utf($dat_veta[47]);
$Smlouva->Provozovatel->Adresa->CisloDomu = win2utf($dat_veta[48]);
$Smlouva->Provozovatel->Adresa->MestskaCast = win2utf($dat_veta[49]);
$Smlouva->Provozovatel->Adresa->CastObce = win2utf($dat_veta[50]);
$Smlouva->Provozovatel->Adresa->Obec = win2utf($dat_veta[51]);
$Smlouva->Provozovatel->Adresa->PSC = win2utf($dat_veta[52]);
$Smlouva->Provozovatel->Adresa->Stat = win2utf($dat_veta[53]);
$Smlouva->Provozovatel->Adresa->UIRid = win2utf($dat_veta[54]);
$Smlouva->Vlastnik = new Subjekt;
$Smlouva->Vlastnik->TitulPred = win2utf($dat_veta[55]);
$Smlouva->Vlastnik->Jmeno = win2utf($dat_veta[56]);
$Smlouva->Vlastnik->Prijmeni = win2utf($dat_veta[57]);
$Smlouva->Vlastnik->RC = win2utf($dat_veta[58]);
$Smlouva->Vlastnik->IC = win2utf($dat_veta[59]);
$Smlouva->Vlastnik->NazevFirmy = win2utf($dat_veta[60]);
$Smlouva->Vlastnik->DatumNarozeni = win2utf($dat_veta[61]);
$Smlouva->Vlastnik->StatPrislusnost = win2utf($dat_veta[62]);
$Smlouva->Vlastnik->Telefon = win2utf($dat_veta[63]);
$Smlouva->Vlastnik->Email = win2utf($dat_veta[64]);
$Smlouva->Vlastnik->TypSubjektu = win2utf($dat_veta[65]);
$Smlouva->Vlastnik->Adresa = new Adresa;
$Smlouva->Vlastnik->Adresa->Ulice = win2utf($dat_veta[66]);
$Smlouva->Vlastnik->Adresa->CisloDomu = win2utf($dat_veta[67]);
$Smlouva->Vlastnik->Adresa->MestskaCast = win2utf($dat_veta[68]);
$Smlouva->Vlastnik->Adresa->CastObce = win2utf($dat_veta[69]);
$Smlouva->Vlastnik->Adresa->Obec = win2utf($dat_veta[70]);
$Smlouva->Vlastnik->Adresa->PSC = win2utf($dat_veta[71]);
$Smlouva->Vlastnik->Adresa->Stat = win2utf($dat_veta[72]);
$Smlouva->Vlastnik->Adresa->UIRid = win2utf($dat_veta[73]);
$Smlouva->Pojisteny = new Subjekt;
$Smlouva->Pojisteny->TitulPred = win2utf($dat_veta[74]);
$Smlouva->Pojisteny->Jmeno = win2utf($dat_veta[75]);
$Smlouva->Pojisteny->Prijmeni = win2utf($dat_veta[76]);
$Smlouva->Pojisteny->RC = win2utf($dat_veta[77]);
$Smlouva->Pojisteny->IC = win2utf($dat_veta[78]);
$Smlouva->Pojisteny->NazevFirmy = win2utf($dat_veta[79]);
$Smlouva->Pojisteny->DatumNarozeni = win2utf($dat_veta[80]);
$Smlouva->Pojisteny->StatPrislusnost = win2utf($dat_veta[81]);
$Smlouva->Pojisteny->Telefon = win2utf($dat_veta[82]);
$Smlouva->Pojisteny->Email = win2utf($dat_veta[83]);
$Smlouva->Pojisteny->TypSubjektu = win2utf($dat_veta[84]);
$Smlouva->Pojisteny->Adresa = new Adresa;
$Smlouva->Pojisteny->Adresa->Ulice = win2utf($dat_veta[85]);
$Smlouva->Pojisteny->Adresa->CisloDomu = win2utf($dat_veta[86]);
$Smlouva->Pojisteny->Adresa->MestskaCast = win2utf($dat_veta[87]);
$Smlouva->Pojisteny->Adresa->CastObce = win2utf($dat_veta[88]);
$Smlouva->Pojisteny->Adresa->Obec = win2utf($dat_veta[89]);
$Smlouva->Pojisteny->Adresa->PSC = win2utf($dat_veta[90]);
$Smlouva->Pojisteny->Adresa->Stat = win2utf($dat_veta[91]);
$Smlouva->Pojisteny->Adresa->UIRid = win2utf($dat_veta[92]);
$Smlouva->KorespondencniAdresa = new Adresa;
$Smlouva->KorespondencniAdresa->Ulice = win2utf($dat_veta[93]);
$Smlouva->KorespondencniAdresa->CisloDomu = win2utf($dat_veta[94]);
$Smlouva->KorespondencniAdresa->MestskaCast = win2utf($dat_veta[95]);
$Smlouva->KorespondencniAdresa->CastObce = win2utf($dat_veta[96]);
$Smlouva->KorespondencniAdresa->Obec = win2utf($dat_veta[97]);
$Smlouva->KorespondencniAdresa->PSC = win2utf($dat_veta[98]);
$Smlouva->KorespondencniAdresa->Stat = win2utf($dat_veta[99]);
$Smlouva->KorespondencniAdresa->UIRid = win2utf($dat_veta[100]);
$Smlouva->VozidloSmlouvy = new VozidloSmlouvy;
$Smlouva->VozidloSmlouvy->SPZ = win2utf($dat_veta[101]);
$Smlouva->VozidloSmlouvy->CisloTP = win2utf($dat_veta[102]);
$Smlouva->VozidloSmlouvy->VIN = win2utf($dat_veta[103]);
$Smlouva->VozidloSmlouvy->PocetMistCelkem = win2utf($dat_veta[104]);
$Smlouva->VozidloSmlouvy->Datum1Registrace = win2utf($dat_veta[105]);
$Smlouva->VozidloSmlouvy->DenDoProvozu = win2utf($dat_veta[106]);
$Smlouva->VozidloSmlouvy->RokDoProvozu = win2utf($dat_veta[107]);
$Smlouva->VozidloSmlouvy->ZnackaText = win2utf($dat_veta[108]);
$Smlouva->VozidloSmlouvy->ModelText = win2utf($dat_veta[109]);
$Smlouva->VozidloSmlouvy->KodZnackyVozidla = win2utf($dat_veta[110]);
$Smlouva->VozidloSmlouvy->KodModeluVozidla = win2utf($dat_veta[111]);
$Smlouva->VozidloSmlouvy->VykonMotoru = win2utf($dat_veta[112]);
$Smlouva->VozidloSmlouvy->ZdvihovyObjem = win2utf($dat_veta[113]);
$Smlouva->VozidloSmlouvy->CelkovaHmotnost = win2utf($dat_veta[114]);
$Smlouva->VozidloSmlouvy->DruhUziti = win2utf($dat_veta[115]);
$Smlouva->VozidloSmlouvy->Leasing = win2utf($dat_veta[116]);
$Smlouva->VozidloSmlouvy->MaxPocetOsob = win2utf($dat_veta[117]);
$Smlouva->VozidloSmlouvy->UjetoCelkem = win2utf($dat_veta[118]);
$Smlouva->VozidloSmlouvy->Palivo = win2utf($dat_veta[119]);
$Smlouva->VozidloSmlouvy->BarvaText = win2utf($dat_veta[120]);
$Smlouva->VozidloSmlouvy->PoradiVozidla = win2utf($dat_veta[121]);
$Smlouva->VozidloSmlouvy->MPZ = win2utf($dat_veta[122]);
$Smlouva->POV = new POV;
$Smlouva->POV->POV = win2utf($dat_veta[123]);
$Smlouva->POV->RozsahKrytiKod = win2utf($dat_veta[124]);
$Smlouva->POV->TarifniSkupinaPojPOV = win2utf($dat_veta[125]);
$Smlouva->POV->SlevyPOV = new SlevyPOV;
$Smlouva->POV->SlevyPOV->HistorickeVozidlo = win2utf($dat_veta[126]);
$Smlouva->POV->SlevyPOV->Invalida = win2utf($dat_veta[127]);
$Smlouva->POV->SlevyPOV->ZimniPneu = win2utf($dat_veta[128]);
$Smlouva->POV->SlevyPOV->Zemedelec = win2utf($dat_veta[129]);
$Smlouva->POV->SlevyPOV->NAStary = win2utf($dat_veta[130]);
$Smlouva->POV->SlevyPOV->Rodina = new Rodina;
$Smlouva->POV->SlevyPOV->Rodina->SlevaRodina = win2utf($dat_veta[131]);
$Smlouva->POV->SlevyPOV->Rodina->CisloSmlouvyRodina = win2utf($dat_veta[132]);
$Smlouva->POV->SlevyPOV->Rodina->PoradiVozidlaRodina = win2utf($dat_veta[133]);
$Smlouva->POV->SlevyPOV->SlevaPOV = win2utf($dat_veta[134]);
$Smlouva->POV->PSP = new PSP;
$Smlouva->POV->PSP->NositelPSP = win2utf($dat_veta[135]);
$Smlouva->POV->PSP->RozhodnaDoba = win2utf($dat_veta[136]);
$Smlouva->POV->PSP->ZdrojovaSmlouva = new ZdrojovaSmlouva;
$Smlouva->POV->PSP->ZdrojovaSmlouva->KodPojistitele = win2utf($dat_veta[137]);
$Smlouva->POV->PSP->ZdrojovaSmlouva->CisloSmlouvy = win2utf($dat_veta[138]);
$Smlouva->POV->PSP->ZdrojovaSmlouva->PoradiVozidla = win2utf($dat_veta[139]);
$Smlouva->POV->Segmentace = new Segmentace;
$Smlouva->POV->Segmentace->VekRidice = win2utf($dat_veta[140]);
$Smlouva->POV->Segmentace->VozIC = win2utf($dat_veta[141]);
$Smlouva->HAV = new HAV;
$Smlouva->HAV->HAV = win2utf($dat_veta[142]);
$Smlouva->HAV->AllRisks = win2utf($dat_veta[143]);
$Smlouva->HAV->Cena = win2utf($dat_veta[144]);
$Smlouva->HAV->PojistnaCastkaHAV = win2utf($dat_veta[145]);
$Smlouva->HAV->VcetneDPH = win2utf($dat_veta[146]);
$Smlouva->HAV->Spoluucast = win2utf($dat_veta[147]);
$Smlouva->HAV->SpoluucastNejm = win2utf($dat_veta[148]);
$Smlouva->HAV->TarifniSkupinaPojHAV = win2utf($dat_veta[149]);
$Smlouva->HAV->SlevyHAV = new SlevyHAV;
$Smlouva->HAV->SlevyHAV->ZabezpMech = win2utf($dat_veta[150]);
$Smlouva->HAV->SlevyHAV->AktivniZabezpeceni = win2utf($dat_veta[151]);
$Smlouva->HAV->SlevyHAV->PasivniZabezpeceni = win2utf($dat_veta[152]);
$Smlouva->HAV->SlevyHAV->Imobilizer = win2utf($dat_veta[153]);
$Smlouva->HAV->SlevyHAV->UplatnitBonusPOV = win2utf($dat_veta[154]);
$Smlouva->HAV->SlevyHAV->PocetMesicu = win2utf($dat_veta[155]);
$Smlouva->HAV->SlevyHAV->BonusPotvrzen = win2utf($dat_veta[156]);
$Smlouva->HAV->SlevyHAV->SlevaHAV = win2utf($dat_veta[157]);
$Smlouva->HAV->Vinkulace = new Vinkulace;
$Smlouva->HAV->Vinkulace->Vinkulace = win2utf($dat_veta[158]);
$Smlouva->HAV->Vinkulace->VeProspech = win2utf($dat_veta[159]);
$Smlouva->HAV->Vinkulace->OdCastky = win2utf($dat_veta[160]);
$Smlouva->HAV->Vinkulace->Predcisli = win2utf($dat_veta[161]);
$Smlouva->HAV->Vinkulace->Ucet = win2utf($dat_veta[162]);
$Smlouva->HAV->Vinkulace->KodBanky = win2utf($dat_veta[163]);
$Smlouva->HAV->Vinkulace->SpecSymbol = win2utf($dat_veta[164]);
$Smlouva->HAV->Prohlidka = new Prohlidka;
$Smlouva->HAV->Prohlidka->DatumProhlidky = win2utf($dat_veta[165]);
$Smlouva->HAV->Prohlidka->CasProhlidky = win2utf($dat_veta[166]);
$Smlouva->HAV->Prohlidka->VozidloPoskozeno = win2utf($dat_veta[167]);
$Smlouva->HAV->PojistneHAVPred = win2utf($dat_veta[168]);
$Smlouva->HAV->PojistneCelkem = win2utf($dat_veta[169]);
$Smlouva->Pripojisteni = new Pripojisteni;
$Smlouva->Pripojisteni->CelniSklo = win2utf($dat_veta[170]);
$Smlouva->Pripojisteni->CelniSkloPC = win2utf($dat_veta[171]);
$Smlouva->Pripojisteni->CelniSkloSpolNejm = win2utf($dat_veta[172]);
$Smlouva->Pripojisteni->CelniSkloSpolPr = win2utf($dat_veta[173]);
$Smlouva->Pripojisteni->PojistneCelniSklo = win2utf($dat_veta[174]);
$Smlouva->Pripojisteni->Asistence = win2utf($dat_veta[175]);
$Smlouva->Pripojisteni->Zivel = win2utf($dat_veta[176]);
$Smlouva->Pripojisteni->VsechnaSkla = win2utf($dat_veta[177]);
$Smlouva->Pripojisteni->VsechnaSklaPC = win2utf($dat_veta[178]);
$Smlouva->Pripojisteni->VsechnaSklaSpolNejm = win2utf($dat_veta[179]);
$Smlouva->Pripojisteni->VsechnaSklaSpolPr = win2utf($dat_veta[180]);
$Smlouva->Pripojisteni->PojistneOkna = win2utf($dat_veta[181]);
$Smlouva->Pripojisteni->PoskozeniSkelCastka = win2utf($dat_veta[182]);
$Smlouva->Pripojisteni->PoskozeniSkelSpolMin = win2utf($dat_veta[183]);
$Smlouva->Pripojisteni->UrazOsob = win2utf($dat_veta[184]);
$Smlouva->Pripojisteni->UrazOsobNasobek = win2utf($dat_veta[185]);
$Smlouva->Pripojisteni->PocetSedadel = win2utf($dat_veta[186]);
$Smlouva->Pripojisteni->PojistneUrazOsob = win2utf($dat_veta[187]);
$Smlouva->Pripojisteni->UrazRidice = win2utf($dat_veta[188]);
$Smlouva->Pripojisteni->UrazRidiceNasobek = win2utf($dat_veta[189]);
$Smlouva->Pripojisteni->PojistneUrazRidice = win2utf($dat_veta[190]);
$Smlouva->Pripojisteni->Zavazadla = win2utf($dat_veta[191]);
$Smlouva->Pripojisteni->PojistneZavazadla = win2utf($dat_veta[192]);
$Smlouva->Pripojisteni->Vybava = new Vybava;
$Smlouva->Pripojisteni->Vybava->Vybava = win2utf($dat_veta[193]);
$Smlouva->Pripojisteni->Vybava->VybavaPojCastka = win2utf($dat_veta[194]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC1 = win2utf($dat_veta[195]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC2 = win2utf($dat_veta[196]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC3 = win2utf($dat_veta[197]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC4 = win2utf($dat_veta[198]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC5 = win2utf($dat_veta[199]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC6 = win2utf($dat_veta[200]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC7 = win2utf($dat_veta[201]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC8 = win2utf($dat_veta[202]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC1 = win2utf($dat_veta[203]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC2 = win2utf($dat_veta[204]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC3 = win2utf($dat_veta[205]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC4 = win2utf($dat_veta[206]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC5 = win2utf($dat_veta[207]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC6 = win2utf($dat_veta[208]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC7 = win2utf($dat_veta[209]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC8 = win2utf($dat_veta[210]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni1 = win2utf($dat_veta[211]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni2 = win2utf($dat_veta[212]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni3 = win2utf($dat_veta[213]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni4 = win2utf($dat_veta[214]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni5 = win2utf($dat_veta[215]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni6 = win2utf($dat_veta[216]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni7 = win2utf($dat_veta[217]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni8 = win2utf($dat_veta[218]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb1 = win2utf($dat_veta[219]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb2 = win2utf($dat_veta[220]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb3 = win2utf($dat_veta[221]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb4 = win2utf($dat_veta[222]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb5 = win2utf($dat_veta[223]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb6 = win2utf($dat_veta[224]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb7 = win2utf($dat_veta[225]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb8 = win2utf($dat_veta[226]);
$Smlouva->Pripojisteni->StretZver = win2utf($dat_veta[227]);
$Smlouva->Pripojisteni->StretZverSpolMin = win2utf($dat_veta[228]);
$Smlouva->Pripojisteni->StretZverSpolPr = win2utf($dat_veta[229]);
$Smlouva->Pripojisteni->StretZverPC = win2utf($dat_veta[230]);
$Smlouva->Pripojisteni->StretZverPojistne = win2utf($dat_veta[231]);
$Smlouva->ZelenaKarta = new ZelenaKarta;
$Smlouva->ZelenaKarta->CisloZK = win2utf($dat_veta[232]);
$Smlouva->ZelenaKarta->PlatnostZKOd = win2utf($dat_veta[233]);
$Smlouva->ZelenaKarta->PlatnostZKDo = win2utf($dat_veta[234]);
$Smlouva->ZelenaKarta->DatumVystaveni = win2utf($dat_veta[235]);
$Smlouva->Makler = win2utf($dat_veta[0]);
$Smlouva->MaklerKod = win2utf($dat_veta[1]);
$Smlouva->ProduktKod = win2utf($dat_veta[2]);
$Smlouva->CisloSmlouvy = win2utf($dat_veta[3]);
$Smlouva->NahrazujeSmlouvu = win2utf($dat_veta[4]);
$Smlouva->CisloRamcoveSmlouvy = win2utf($dat_veta[5]);
$Smlouva->ZpusobPlatby = win2utf($dat_veta[6]);
$Smlouva->SipoSpojeni = win2utf($dat_veta[7]);
$Smlouva->PocetSplatek = win2utf($dat_veta[8]);
$Smlouva->DatumPocatku = win2utf($dat_veta[9]);
$Smlouva->HodinaPocatku = win2utf($dat_veta[10]);
$Smlouva->DatumKoncePojisteni = win2utf($dat_veta[11]);
$Smlouva->DatumUzavreni = win2utf($dat_veta[12]);
$Smlouva->UpraveneRocniPojistne = win2utf($dat_veta[13]);
$Smlouva->ObchodniSleva = win2utf($dat_veta[14]);
$Smlouva->PrirazkaSplatnost = win2utf($dat_veta[15]);
$Smlouva->SlevaPropojistenost = win2utf($dat_veta[16]);
$Smlouva->Pojistnik = new Subjekt;
$Smlouva->Pojistnik->TitulPred = win2utf($dat_veta[17]);
$Smlouva->Pojistnik->Jmeno = win2utf($dat_veta[18]);
$Smlouva->Pojistnik->Prijmeni = win2utf($dat_veta[19]);
$Smlouva->Pojistnik->RC = win2utf($dat_veta[20]);
$Smlouva->Pojistnik->IC = win2utf($dat_veta[21]);
$Smlouva->Pojistnik->NazevFirmy = win2utf($dat_veta[22]);
$Smlouva->Pojistnik->DatumNarozeni = win2utf($dat_veta[23]);
$Smlouva->Pojistnik->StatPrislusnost = win2utf($dat_veta[24]);
$Smlouva->Pojistnik->Telefon = win2utf($dat_veta[25]);
$Smlouva->Pojistnik->Email = win2utf($dat_veta[26]);
$Smlouva->Pojistnik->TypSubjektu = win2utf($dat_veta[27]);
$Smlouva->Pojistnik->Adresa = new Adresa;
$Smlouva->Pojistnik->Adresa->Ulice = win2utf($dat_veta[28]);
$Smlouva->Pojistnik->Adresa->CisloDomu = win2utf($dat_veta[29]);
$Smlouva->Pojistnik->Adresa->MestskaCast = win2utf($dat_veta[30]);
$Smlouva->Pojistnik->Adresa->CastObce = win2utf($dat_veta[31]);
$Smlouva->Pojistnik->Adresa->Obec = win2utf($dat_veta[32]);
$Smlouva->Pojistnik->Adresa->PSC = win2utf($dat_veta[33]);
$Smlouva->Pojistnik->Adresa->Stat = win2utf($dat_veta[34]);
$Smlouva->Pojistnik->Adresa->UIRid = win2utf($dat_veta[35]);
$Smlouva->Provozovatel = new Subjekt;
$Smlouva->Provozovatel->TitulPred = win2utf($dat_veta[36]);
$Smlouva->Provozovatel->Jmeno = win2utf($dat_veta[37]);
$Smlouva->Provozovatel->Prijmeni = win2utf($dat_veta[38]);
$Smlouva->Provozovatel->RC = win2utf($dat_veta[39]);
$Smlouva->Provozovatel->IC = win2utf($dat_veta[40]);
$Smlouva->Provozovatel->NazevFirmy = win2utf($dat_veta[41]);
$Smlouva->Provozovatel->DatumNarozeni = win2utf($dat_veta[42]);
$Smlouva->Provozovatel->StatPrislusnost = win2utf($dat_veta[43]);
$Smlouva->Provozovatel->Telefon = win2utf($dat_veta[44]);
$Smlouva->Provozovatel->Email = win2utf($dat_veta[45]);
$Smlouva->Provozovatel->TypSubjektu = win2utf($dat_veta[46]);
$Smlouva->Provozovatel->Adresa = new Adresa;
$Smlouva->Provozovatel->Adresa->Ulice = win2utf($dat_veta[47]);
$Smlouva->Provozovatel->Adresa->CisloDomu = win2utf($dat_veta[48]);
$Smlouva->Provozovatel->Adresa->MestskaCast = win2utf($dat_veta[49]);
$Smlouva->Provozovatel->Adresa->CastObce = win2utf($dat_veta[50]);
$Smlouva->Provozovatel->Adresa->Obec = win2utf($dat_veta[51]);
$Smlouva->Provozovatel->Adresa->PSC = win2utf($dat_veta[52]);
$Smlouva->Provozovatel->Adresa->Stat = win2utf($dat_veta[53]);
$Smlouva->Provozovatel->Adresa->UIRid = win2utf($dat_veta[54]);
$Smlouva->Vlastnik = new Subjekt;
$Smlouva->Vlastnik->TitulPred = win2utf($dat_veta[55]);
$Smlouva->Vlastnik->Jmeno = win2utf($dat_veta[56]);
$Smlouva->Vlastnik->Prijmeni = win2utf($dat_veta[57]);
$Smlouva->Vlastnik->RC = win2utf($dat_veta[58]);
$Smlouva->Vlastnik->IC = win2utf($dat_veta[59]);
$Smlouva->Vlastnik->NazevFirmy = win2utf($dat_veta[60]);
$Smlouva->Vlastnik->DatumNarozeni = win2utf($dat_veta[61]);
$Smlouva->Vlastnik->StatPrislusnost = win2utf($dat_veta[62]);
$Smlouva->Vlastnik->Telefon = win2utf($dat_veta[63]);
$Smlouva->Vlastnik->Email = win2utf($dat_veta[64]);
$Smlouva->Vlastnik->TypSubjektu = win2utf($dat_veta[65]);
$Smlouva->Vlastnik->Adresa = new Adresa;
$Smlouva->Vlastnik->Adresa->Ulice = win2utf($dat_veta[66]);
$Smlouva->Vlastnik->Adresa->CisloDomu = win2utf($dat_veta[67]);
$Smlouva->Vlastnik->Adresa->MestskaCast = win2utf($dat_veta[68]);
$Smlouva->Vlastnik->Adresa->CastObce = win2utf($dat_veta[69]);
$Smlouva->Vlastnik->Adresa->Obec = win2utf($dat_veta[70]);
$Smlouva->Vlastnik->Adresa->PSC = win2utf($dat_veta[71]);
$Smlouva->Vlastnik->Adresa->Stat = win2utf($dat_veta[72]);
$Smlouva->Vlastnik->Adresa->UIRid = win2utf($dat_veta[73]);
$Smlouva->Pojisteny = new Subjekt;
$Smlouva->Pojisteny->TitulPred = win2utf($dat_veta[74]);
$Smlouva->Pojisteny->Jmeno = win2utf($dat_veta[75]);
$Smlouva->Pojisteny->Prijmeni = win2utf($dat_veta[76]);
$Smlouva->Pojisteny->RC = win2utf($dat_veta[77]);
$Smlouva->Pojisteny->IC = win2utf($dat_veta[78]);
$Smlouva->Pojisteny->NazevFirmy = win2utf($dat_veta[79]);
$Smlouva->Pojisteny->DatumNarozeni = win2utf($dat_veta[80]);
$Smlouva->Pojisteny->StatPrislusnost = win2utf($dat_veta[81]);
$Smlouva->Pojisteny->Telefon = win2utf($dat_veta[82]);
$Smlouva->Pojisteny->Email = win2utf($dat_veta[83]);
$Smlouva->Pojisteny->TypSubjektu = win2utf($dat_veta[84]);
$Smlouva->Pojisteny->Adresa = new Adresa;
$Smlouva->Pojisteny->Adresa->Ulice = win2utf($dat_veta[85]);
$Smlouva->Pojisteny->Adresa->CisloDomu = win2utf($dat_veta[86]);
$Smlouva->Pojisteny->Adresa->MestskaCast = win2utf($dat_veta[87]);
$Smlouva->Pojisteny->Adresa->CastObce = win2utf($dat_veta[88]);
$Smlouva->Pojisteny->Adresa->Obec = win2utf($dat_veta[89]);
$Smlouva->Pojisteny->Adresa->PSC = win2utf($dat_veta[90]);
$Smlouva->Pojisteny->Adresa->Stat = win2utf($dat_veta[91]);
$Smlouva->Pojisteny->Adresa->UIRid = win2utf($dat_veta[92]);
$Smlouva->KorespondencniAdresa = new Adresa;
$Smlouva->KorespondencniAdresa->Ulice = win2utf($dat_veta[93]);
$Smlouva->KorespondencniAdresa->CisloDomu = win2utf($dat_veta[94]);
$Smlouva->KorespondencniAdresa->MestskaCast = win2utf($dat_veta[95]);
$Smlouva->KorespondencniAdresa->CastObce = win2utf($dat_veta[96]);
$Smlouva->KorespondencniAdresa->Obec = win2utf($dat_veta[97]);
$Smlouva->KorespondencniAdresa->PSC = win2utf($dat_veta[98]);
$Smlouva->KorespondencniAdresa->Stat = win2utf($dat_veta[99]);
$Smlouva->KorespondencniAdresa->UIRid = win2utf($dat_veta[100]);
$Smlouva->VozidloSmlouvy = new VozidloSmlouvy;
$Smlouva->VozidloSmlouvy->SPZ = win2utf($dat_veta[101]);
$Smlouva->VozidloSmlouvy->CisloTP = win2utf($dat_veta[102]);
$Smlouva->VozidloSmlouvy->VIN = win2utf($dat_veta[103]);
$Smlouva->VozidloSmlouvy->PocetMistCelkem = win2utf($dat_veta[104]);
$Smlouva->VozidloSmlouvy->Datum1Registrace = win2utf($dat_veta[105]);
$Smlouva->VozidloSmlouvy->DenDoProvozu = win2utf($dat_veta[106]);
$Smlouva->VozidloSmlouvy->RokDoProvozu = win2utf($dat_veta[107]);
$Smlouva->VozidloSmlouvy->ZnackaText = win2utf($dat_veta[108]);
$Smlouva->VozidloSmlouvy->ModelText = win2utf($dat_veta[109]);
$Smlouva->VozidloSmlouvy->KodZnackyVozidla = win2utf($dat_veta[110]);
$Smlouva->VozidloSmlouvy->KodModeluVozidla = win2utf($dat_veta[111]);
$Smlouva->VozidloSmlouvy->VykonMotoru = win2utf($dat_veta[112]);
$Smlouva->VozidloSmlouvy->ZdvihovyObjem = win2utf($dat_veta[113]);
$Smlouva->VozidloSmlouvy->CelkovaHmotnost = win2utf($dat_veta[114]);
$Smlouva->VozidloSmlouvy->DruhUziti = win2utf($dat_veta[115]);
$Smlouva->VozidloSmlouvy->Leasing = win2utf($dat_veta[116]);
$Smlouva->VozidloSmlouvy->MaxPocetOsob = win2utf($dat_veta[117]);
$Smlouva->VozidloSmlouvy->UjetoCelkem = win2utf($dat_veta[118]);
$Smlouva->VozidloSmlouvy->Palivo = win2utf($dat_veta[119]);
$Smlouva->VozidloSmlouvy->BarvaText = win2utf($dat_veta[120]);
$Smlouva->VozidloSmlouvy->PoradiVozidla = win2utf($dat_veta[121]);
$Smlouva->VozidloSmlouvy->MPZ = win2utf($dat_veta[122]);
$Smlouva->POV = new POV;
$Smlouva->POV->POV = win2utf($dat_veta[123]);
$Smlouva->POV->RozsahKrytiKod = win2utf($dat_veta[124]);
$Smlouva->POV->TarifniSkupinaPojPOV = win2utf($dat_veta[125]);
$Smlouva->POV->SlevyPOV = new SlevyPOV;
$Smlouva->POV->SlevyPOV->HistorickeVozidlo = win2utf($dat_veta[126]);
$Smlouva->POV->SlevyPOV->Invalida = win2utf($dat_veta[127]);
$Smlouva->POV->SlevyPOV->ZimniPneu = win2utf($dat_veta[128]);
$Smlouva->POV->SlevyPOV->Zemedelec = win2utf($dat_veta[129]);
$Smlouva->POV->SlevyPOV->NAStary = win2utf($dat_veta[130]);
$Smlouva->POV->SlevyPOV->Rodina = new Rodina;
$Smlouva->POV->SlevyPOV->Rodina->SlevaRodina = win2utf($dat_veta[131]);
$Smlouva->POV->SlevyPOV->Rodina->CisloSmlouvyRodina = win2utf($dat_veta[132]);
$Smlouva->POV->SlevyPOV->Rodina->PoradiVozidlaRodina = win2utf($dat_veta[133]);
$Smlouva->POV->SlevyPOV->SlevaPOV = win2utf($dat_veta[134]);
$Smlouva->POV->PSP = new PSP;
$Smlouva->POV->PSP->NositelPSP = win2utf($dat_veta[135]);
$Smlouva->POV->PSP->RozhodnaDoba = win2utf($dat_veta[136]);
$Smlouva->POV->PSP->ZdrojovaSmlouva = new ZdrojovaSmlouva;
$Smlouva->POV->PSP->ZdrojovaSmlouva->KodPojistitele = win2utf($dat_veta[137]);
$Smlouva->POV->PSP->ZdrojovaSmlouva->CisloSmlouvy = win2utf($dat_veta[138]);
$Smlouva->POV->PSP->ZdrojovaSmlouva->PoradiVozidla = win2utf($dat_veta[139]);
$Smlouva->POV->Segmentace = new Segmentace;
$Smlouva->POV->Segmentace->VekRidice = win2utf($dat_veta[140]);
$Smlouva->POV->Segmentace->VozIC = win2utf($dat_veta[141]);
$Smlouva->HAV = new HAV;
$Smlouva->HAV->HAV = win2utf($dat_veta[142]);
$Smlouva->HAV->AllRisks = win2utf($dat_veta[143]);
$Smlouva->HAV->Cena = win2utf($dat_veta[144]);
$Smlouva->HAV->PojistnaCastkaHAV = win2utf($dat_veta[145]);
$Smlouva->HAV->VcetneDPH = win2utf($dat_veta[146]);
$Smlouva->HAV->Spoluucast = win2utf($dat_veta[147]);
$Smlouva->HAV->SpoluucastNejm = win2utf($dat_veta[148]);
$Smlouva->HAV->TarifniSkupinaPojHAV = win2utf($dat_veta[149]);
$Smlouva->HAV->SlevyHAV = new SlevyHAV;
$Smlouva->HAV->SlevyHAV->ZabezpMech = win2utf($dat_veta[150]);
$Smlouva->HAV->SlevyHAV->AktivniZabezpeceni = win2utf($dat_veta[151]);
$Smlouva->HAV->SlevyHAV->PasivniZabezpeceni = win2utf($dat_veta[152]);
$Smlouva->HAV->SlevyHAV->Imobilizer = win2utf($dat_veta[153]);
$Smlouva->HAV->SlevyHAV->UplatnitBonusPOV = win2utf($dat_veta[154]);
$Smlouva->HAV->SlevyHAV->PocetMesicu = win2utf($dat_veta[155]);
$Smlouva->HAV->SlevyHAV->BonusPotvrzen = win2utf($dat_veta[156]);
$Smlouva->HAV->SlevyHAV->SlevaHAV = win2utf($dat_veta[157]);
$Smlouva->HAV->Vinkulace = new Vinkulace;
$Smlouva->HAV->Vinkulace->Vinkulace = win2utf($dat_veta[158]);
$Smlouva->HAV->Vinkulace->VeProspech = win2utf($dat_veta[159]);
$Smlouva->HAV->Vinkulace->OdCastky = win2utf($dat_veta[160]);
$Smlouva->HAV->Vinkulace->Predcisli = win2utf($dat_veta[161]);
$Smlouva->HAV->Vinkulace->Ucet = win2utf($dat_veta[162]);
$Smlouva->HAV->Vinkulace->KodBanky = win2utf($dat_veta[163]);
$Smlouva->HAV->Vinkulace->SpecSymbol = win2utf($dat_veta[164]);
$Smlouva->HAV->Prohlidka = new Prohlidka;
$Smlouva->HAV->Prohlidka->DatumProhlidky = win2utf($dat_veta[165]);
$Smlouva->HAV->Prohlidka->CasProhlidky = win2utf($dat_veta[166]);
$Smlouva->HAV->Prohlidka->VozidloPoskozeno = win2utf($dat_veta[167]);
$Smlouva->HAV->PojistneHAVPred = win2utf($dat_veta[168]);
$Smlouva->HAV->PojistneCelkem = win2utf($dat_veta[169]);
$Smlouva->Pripojisteni = new Pripojisteni;
$Smlouva->Pripojisteni->CelniSklo = win2utf($dat_veta[170]);
$Smlouva->Pripojisteni->CelniSkloPC = win2utf($dat_veta[171]);
$Smlouva->Pripojisteni->CelniSkloSpolNejm = win2utf($dat_veta[172]);
$Smlouva->Pripojisteni->CelniSkloSpolPr = win2utf($dat_veta[173]);
$Smlouva->Pripojisteni->PojistneCelniSklo = win2utf($dat_veta[174]);
$Smlouva->Pripojisteni->Asistence = win2utf($dat_veta[175]);
$Smlouva->Pripojisteni->Zivel = win2utf($dat_veta[176]);
$Smlouva->Pripojisteni->VsechnaSkla = win2utf($dat_veta[177]);
$Smlouva->Pripojisteni->VsechnaSklaPC = win2utf($dat_veta[178]);
$Smlouva->Pripojisteni->VsechnaSklaSpolNejm = win2utf($dat_veta[179]);
$Smlouva->Pripojisteni->VsechnaSklaSpolPr = win2utf($dat_veta[180]);
$Smlouva->Pripojisteni->PojistneOkna = win2utf($dat_veta[181]);
$Smlouva->Pripojisteni->PoskozeniSkelCastka = win2utf($dat_veta[182]);
$Smlouva->Pripojisteni->PoskozeniSkelSpolMin = win2utf($dat_veta[183]);
$Smlouva->Pripojisteni->UrazOsob = win2utf($dat_veta[184]);
$Smlouva->Pripojisteni->UrazOsobNasobek = win2utf($dat_veta[185]);
$Smlouva->Pripojisteni->PocetSedadel = win2utf($dat_veta[186]);
$Smlouva->Pripojisteni->PojistneUrazOsob = win2utf($dat_veta[187]);
$Smlouva->Pripojisteni->UrazRidice = win2utf($dat_veta[188]);
$Smlouva->Pripojisteni->UrazRidiceNasobek = win2utf($dat_veta[189]);
$Smlouva->Pripojisteni->PojistneUrazRidice = win2utf($dat_veta[190]);
$Smlouva->Pripojisteni->Zavazadla = win2utf($dat_veta[191]);
$Smlouva->Pripojisteni->PojistneZavazadla = win2utf($dat_veta[192]);
$Smlouva->Pripojisteni->Vybava = new Vybava;
$Smlouva->Pripojisteni->Vybava->Vybava = win2utf($dat_veta[193]);
$Smlouva->Pripojisteni->Vybava->VybavaPojCastka = win2utf($dat_veta[194]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC1 = win2utf($dat_veta[195]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC2 = win2utf($dat_veta[196]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC3 = win2utf($dat_veta[197]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC4 = win2utf($dat_veta[198]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC5 = win2utf($dat_veta[199]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC6 = win2utf($dat_veta[200]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC7 = win2utf($dat_veta[201]);
$Smlouva->Pripojisteni->Vybava->VybavaZahrnutaPC8 = win2utf($dat_veta[202]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC1 = win2utf($dat_veta[203]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC2 = win2utf($dat_veta[204]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC3 = win2utf($dat_veta[205]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC4 = win2utf($dat_veta[206]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC5 = win2utf($dat_veta[207]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC6 = win2utf($dat_veta[208]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC7 = win2utf($dat_veta[209]);
$Smlouva->Pripojisteni->Vybava->CenaVybZahrnutaPC8 = win2utf($dat_veta[210]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni1 = win2utf($dat_veta[211]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni2 = win2utf($dat_veta[212]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni3 = win2utf($dat_veta[213]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni4 = win2utf($dat_veta[214]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni5 = win2utf($dat_veta[215]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni6 = win2utf($dat_veta[216]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni7 = win2utf($dat_veta[217]);
$Smlouva->Pripojisteni->Vybava->VybavaSpecialni8 = win2utf($dat_veta[218]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb1 = win2utf($dat_veta[219]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb2 = win2utf($dat_veta[220]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb3 = win2utf($dat_veta[221]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb4 = win2utf($dat_veta[222]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb5 = win2utf($dat_veta[223]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb6 = win2utf($dat_veta[224]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb7 = win2utf($dat_veta[225]);
$Smlouva->Pripojisteni->Vybava->CenaPolSpecVyb8 = win2utf($dat_veta[226]);
$Smlouva->Pripojisteni->StretZver = win2utf($dat_veta[227]);
$Smlouva->Pripojisteni->StretZverSpolMin = win2utf($dat_veta[228]);
$Smlouva->Pripojisteni->StretZverSpolPr = win2utf($dat_veta[229]);
$Smlouva->Pripojisteni->StretZverPC = win2utf($dat_veta[230]);
$Smlouva->Pripojisteni->StretZverPojistne = win2utf($dat_veta[231]);
$Smlouva->ZelenaKarta = new ZelenaKarta;
$Smlouva->ZelenaKarta->CisloZK = win2utf($dat_veta[232]);
$Smlouva->ZelenaKarta->PlatnostZKOd = win2utf($dat_veta[233]);
$Smlouva->ZelenaKarta->PlatnostZKDo = win2utf($dat_veta[234]);
$Smlouva->ZelenaKarta->DatumVystaveni = win2utf($dat_veta[235]);
