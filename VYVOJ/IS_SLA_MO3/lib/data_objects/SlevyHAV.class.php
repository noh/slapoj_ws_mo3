<?php

/**
  * SlevyHAV
  *
  * SlevyHAV
  *
  */
class SlevyHAV {
   /** @var string */
   public $AktivniZabezpeceni;

   /** @var string */
   public $BonusPotvrzen;

   /** @var string */
   public $Imobilizer;

   /** @var string */
   public $PasivniZabezpeceni;

   /** @var int */
   public $PocetMesicu;

   /** @var int */
   public $SlevaHAV;

   /** @var string */
   public $UplatnitBonusPOV;

   /** @var string */
   public $ZabezpMech;

}
?>