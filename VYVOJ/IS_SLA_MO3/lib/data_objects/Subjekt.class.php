<?php

/**
  * Subjekt
  *
  * Subjekt
  *
  */
class Subjekt {
   /** @var Adresa */
   public $Adresa;

   /** @var string */
   public $DatumNarozeni;

   /** @var string */
   public $Email;

   /** @var int */
   public $IC;

   /** @var string */
   public $Jmeno;

   /** @var string */
   public $NazevFirmy;

   /** @var string */
   public $Prijmeni;

   /** @var string */
   public $RC;

   /** @var string */
   public $StatPrislusnost;

   /** @var string */
   public $Telefon;

   /** @var string */
   public $TitulPred;

   /** @var string */
   public $TypSubjektu;

}
?>