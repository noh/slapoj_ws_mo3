<?php

/**
  * UkonceniIn
  *
  * UkonceniIn
  *
  */
class UkonceniIn {
   /** @var string */
   public $CisloSmlouvy;

   /** @var int */
   public $DatumUkonceni;

   /** @var string */
   public $KodUkonceniDuvod;

   /** @var string */
   public $KodUkonceniNarok;

   /** @var int */
   public $PojisteniDo;

   /** @var int */
   public $PoradiVozidla;

   /** @var string */
   public $VracenaZK;

}
?>