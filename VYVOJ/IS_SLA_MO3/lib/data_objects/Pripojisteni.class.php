<?php

/**
  * Pripojisteni
  *
  * Pripojisteni
  *
  */
class Pripojisteni {
   /** @var string */
   public $Asistence;

   /** @var string */
   public $CelniSklo;

   /** @var int */
   public $CelniSkloPC;

   /** @var int */
   public $CelniSkloSpolNejm;

   /** @var int */
   public $CelniSkloSpolPr;

   /** @var int */
   public $PocetSedadel;

   /** @var int */
   public $PojistneCelniSklo;

   /** @var int */
   public $PojistneOkna;

   /** @var int */
   public $PojistneUrazOsob;

   /** @var int */
   public $PojistneUrazRidice;

   /** @var int */
   public $PojistneZavazadla;

   /** @var int */
   public $PoskozeniSkelCastka;

   /** @var int */
   public $PoskozeniSkelSpolMin;

   /** @var string */
   public $StretZver;

   /** @var int */
   public $StretZverPC;

   /** @var int */
   public $StretZverPojistne;

   /** @var int */
   public $StretZverSpolMin;

   /** @var int */
   public $StretZverSpolPr;

   /** @var string */
   public $UrazOsob;

   /** @var int */
   public $UrazOsobNasobek;

   /** @var string */
   public $UrazRidice;

   /** @var int */
   public $UrazRidiceNasobek;

   /** @var string */
   public $VsechnaSkla;

   /** @var int */
   public $VsechnaSklaPC;

   /** @var int */
   public $VsechnaSklaSpolNejm;

   /** @var int */
   public $VsechnaSklaSpolPr;

   /** @var Vybava */
   public $Vybava;

   /** @var string */
   public $Zavazadla;

   /** @var string */
   public $Zivel;

   /** @var int */
   public $ZivelPC;

   /** @var int */
   public $ZivelSpolNejm;

   /** @var int */
   public $ZivelSpolPr;

}
?>