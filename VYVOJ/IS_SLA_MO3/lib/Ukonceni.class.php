<?php

class Ukonceni {

   public function ImportUkonceni($CisloSmlouvy, $PoradiVozidla, $DatumZmeny, $PojisteniDo, $KodUkonceniDuvod, $KodUkonceniNarok, $VracenaZK) {
     $Vysledek = new UkonceniOut;
     $mdb_conn=odbc_connect(Conf::db_name, Conf::db_user, Conf::db_passw);
     odbc_setoption($mdb_conn,1,SQL_ATTR_COMMIT,SQL_TXN_NO_COMMIT);
     //Zjištění posledního ID
     $mdb_com = "select max(RWSZID) from " . Conf::db_RWSZ;
	   $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
	   if (odbc_fetch_row($mdb_rslt)) {
	     $IDx = odbc_result($mdb_rslt, 1);
	     if ($IDx == '') {
	       $IDx = 1;
			 } else {
	       $IDx++;
	     }
	   } else {
		   $Vysledek = $this->AddError($Vysledek, "Interní chyba, nelze načíst ID!");
		   return $Vysledek;
		 }

     $mdb_com = "INSERT INTO " . Conf::db_RWSZ . " (RWSZID, QDESIN, QVEHID, RWSZTY, RWSZDT, RWSZDO, RWSZKD, RWSZKN, RWSZZK) VALUES($IDx, $CisloSmlouvy, $PoradiVozidla, 'UK', $DatumZmeny, $PojisteniDo, '$KodUkonceniDuvod', '$KodUkonceniNarok', '$VracenaZK')";
     $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
     
     if (!$mdb_rslt) {
		    $Vysledek = $this->AddError($Vysledek, "Nelze předat požadavek - pravděpodobně chybná data!");
		    return $Vysledek;
		 }
     
     //Volání LANSA kontrolní fce
     $conn = i5_connect(Conf::i5_server, Conf::i5_user, Conf::i5_pasw);
     if (!$conn) {
        $Vysledek = $this->AddError($Vysledek, "RWSSCHG - Interní chyba - connect!");
        return $Vysledek;
     }
     $cmd = i5_command("LANSA REQUEST(RUN) PROCESS(RWSSPR01) FUNCTION(RWSSCHG) PARTITION('VES') LANGUAGE('CZE ')");
     if (!$cmd) {
       $Vysledek = $this->AddError($Vysledek, "RWSSCHG - Interní chyba - Volání RWSSCHG!");
       return $Vysledek;
     }
  
     //Přečtení výsledku
     $mdb_com = "select * from " . Conf::db_RWSZ . " where RWSZID = $IDx";
	   $mdb_rslt = odbc_exec($mdb_conn, $mdb_com);
	   if (odbc_fetch_row($mdb_rslt)) {
	     $Vysledek->Status = odbc_result($mdb_rslt, "RWSZST");
	     $Vysledek->TextovaZprava = iconv("ISO8859-2", "UTF-8", trim(odbc_result($mdb_rslt, "RWSZMS")));
	     $Vysledek->PredpisOd = odbc_result($mdb_rslt, "RWSZPO");
	     $Vysledek->PredpisDo = odbc_result($mdb_rslt, "RWSZPD");
	     $Vysledek->PredpisSuma = odbc_result($mdb_rslt, "RWSZPP");
	     $Vysledek->BankaSuma = odbc_result($mdb_rslt, "RWSZPE");
	   } else {
		   $Vysledek = $this->AddError($Vysledek, "RWSZ - Interní chyba, nelze načíst odpověď dle ID!");
		 }
     return $Vysledek;
   }

	private function AddError ($Vysledek, $Popis) {
      $Vysledek->TextovaZprava = $Popis;
      $Vysledek->Status = "ER";
      return $Vysledek;
	}

}
?>