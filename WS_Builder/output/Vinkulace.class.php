<?php

/**
  * Vinkulace
  *
  * Vinkulace
  *
  */
class Vinkulace {
   /** @var int */
   public $KodBanky;

   /** @var int */
   public $OdCastky;

   /** @var int */
   public $Predcisli;

   /** @var int */
   public $SpecSymbol;

   /** @var int */
   public $Ucet;

   /** @var string */
   public $VeProspech;

   /** @var string */
   public $Vinkulace;

}
?>