<?php

/**
  * VozidloSmlouvy
  *
  * VozidloSmlouvy
  *
  */
class VozidloSmlouvy {
   /** @var string */
   public $BarvaText;

   /** @var int */
   public $CelkovaHmotnost;

   /** @var string */
   public $CisloTP;

   /** @var string */
   public $Datum1Registrace;

   /** @var int */
   public $DenDoProvozu;

   /** @var string */
   public $DruhUziti;

   /** @var int */
   public $KodModeluVozidla;

   /** @var int */
   public $KodZnackyVozidla;

   /** @var string */
   public $Leasing;

   /** @var int */
   public $MaxPocetOsob;

   /** @var string */
   public $ModelText;

   /** @var string */
   public $MPZ;

   /** @var string */
   public $Palivo;

   /** @var int */
   public $PocetMistCelkem;

   /** @var int */
   public $PoradiVozidla;

   /** @var int */
   public $RokDoProvozu;

   /** @var string */
   public $SPZ;

   /** @var int */
   public $UjetoCelkem;

   /** @var string */
   public $VIN;

   /** @var int */
   public $VykonMotoru;

   /** @var int */
   public $ZdvihovyObjem;

   /** @var string */
   public $ZnackaText;

}
?>