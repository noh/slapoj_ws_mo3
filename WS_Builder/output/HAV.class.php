<?php

/**
  * HAV
  *
  * HAV
  *
  */
class HAV {
   /** @var string */
   public $AllRisks;

   /** @var string */
   public $Cena;

   /** @var string */
   public $HAV;

   /** @var int */
   public $PojistnaCastkaHAV;

   /** @var int */
   public $PojistneCelkem;

   /** @var int */
   public $PojistneHAVPred;

   /** @var Prohlidka */
   public $Prohlidka;

   /** @var SlevyHAV */
   public $SlevyHAV;

   /** @var int */
   public $Spoluucast;

   /** @var int */
   public $SpoluucastNejm;

   /** @var string */
   public $TarifniSkupinaPojHAV;

   /** @var string */
   public $VcetneDPH;

   /** @var Vinkulace */
   public $Vinkulace;

}
?>