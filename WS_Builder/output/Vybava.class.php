<?php

/**
  * Vybava
  *
  * Vybava
  *
  */
class Vybava {
   /** @var int */
   public $CenaPolSpecVyb1;

   /** @var int */
   public $CenaPolSpecVyb2;

   /** @var int */
   public $CenaPolSpecVyb3;

   /** @var int */
   public $CenaPolSpecVyb4;

   /** @var int */
   public $CenaPolSpecVyb5;

   /** @var int */
   public $CenaPolSpecVyb6;

   /** @var int */
   public $CenaPolSpecVyb7;

   /** @var int */
   public $CenaPolSpecVyb8;

   /** @var int */
   public $CenaVybZahrnutaPC1;

   /** @var int */
   public $CenaVybZahrnutaPC2;

   /** @var int */
   public $CenaVybZahrnutaPC3;

   /** @var int */
   public $CenaVybZahrnutaPC4;

   /** @var int */
   public $CenaVybZahrnutaPC5;

   /** @var int */
   public $CenaVybZahrnutaPC6;

   /** @var int */
   public $CenaVybZahrnutaPC7;

   /** @var int */
   public $CenaVybZahrnutaPC8;

   /** @var string */
   public $Vybava;

   /** @var int */
   public $VybavaPojCastka;

   /** @var string */
   public $VybavaSpecialni1;

   /** @var string */
   public $VybavaSpecialni2;

   /** @var string */
   public $VybavaSpecialni3;

   /** @var string */
   public $VybavaSpecialni4;

   /** @var string */
   public $VybavaSpecialni5;

   /** @var string */
   public $VybavaSpecialni6;

   /** @var string */
   public $VybavaSpecialni7;

   /** @var string */
   public $VybavaSpecialni8;

   /** @var string */
   public $VybavaZahrnutaPC1;

   /** @var string */
   public $VybavaZahrnutaPC2;

   /** @var string */
   public $VybavaZahrnutaPC3;

   /** @var string */
   public $VybavaZahrnutaPC4;

   /** @var string */
   public $VybavaZahrnutaPC5;

   /** @var string */
   public $VybavaZahrnutaPC6;

   /** @var string */
   public $VybavaZahrnutaPC7;

   /** @var string */
   public $VybavaZahrnutaPC8;

}
?>