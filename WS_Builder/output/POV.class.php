<?php

/**
  * POV
  *
  * POV
  *
  */
class POV {
   /** @var string */
   public $POV;

   /** @var PSP */
   public $PSP;

   /** @var string */
   public $RozsahKrytiKod;

   /** @var Segmentace */
   public $Segmentace;

   /** @var SlevyPOV */
   public $SlevyPOV;

   /** @var string */
   public $TarifniSkupinaPojPOV;

}
?>