<?php

require('lib/conf.php');
require('lib/db_def.php');
require('lib/convert.php');
require('fpdf/fpdf.php');

//Parametry str�nky
$vys = 297; //V��ka str�nky
$sir = 210; //���ka str�nky
$okrx = 20; //Okraj str�nky (lev�, prav�)
$okrt = 40; //Okraj str�nky (Vr�ek)
$okrb = 25; //Okraj str�nky (spodek)
$mokr = 5; //Men�� okraj str�nky
$ods = 15; //Odsazen�

class PDF extends FPDF
{
var $left_def = 0;
var $right_def = 0;
var $top_def = 0;
var $doc_name = '';

function Header()
{
    $this->Image('fpdf/img/logo.png', 25, 10, 60);
    $this->SetXY(90, 19);
    $this->SetTextColor(30, 60, 100);
    $this->SetFont('Arial-BoldMT','',12);
    $this->Write(6, $this->doc_name);
    $this->Line(25, 25, 180, 25);
    $this->SetTextColor(0, 0, 0);
    $this->SetMargins($this->left_def, $this->top_def, $this->right_def);
    $this->SetXY($this->left_def, $this->top_def);
}
function Footer()
{
    $this->SetY(-15);
    $this->SetFont('Arial','I',8);
    $this->Cell(0,10,'- '.$this->PageNo(). ' -',0,0,'C');
}
public function Nadpis1($text){
   $this->SetTextColor(30, 60, 100);
   $this->SetFont('Arial-BoldMT','','18');
   $this->MultiCell(0,6, $text, 0, 'L');
   $this->SetTextColor(0, 0, 0);
   $this->Ln();
}
public function Nadpis2($text){
   $this->Ln();
   $this->SetTextColor(30, 60, 100);
   $this->SetFont('Arial-BoldMT','','16');
   $this->MultiCell(0,6, $text, 0, 'L');
   $this->SetTextColor(0, 0, 0);
   $this->Ln();
}
public function Odstavec($text){
   $this->SetFont('ArialMT','','12');
   $this->MultiCell(0,6, $text, 0, 'J');
   $this->Ln();
}
public function Definice($Vystup, $Jmeno, $Vstup){
   $this->Ln();
   $this->MultiCell(0,6, "Definice:", 0, 'J');
   $this->SetX($this->left_def + 15);
   $this->SetFont('arial','I','12');
   $this->Write(6, $Vystup . " ");
   $this->SetFont('arial','B','12');
   $this->Write(6, $Jmeno . " ");
   $this->SetFont('arial','I','12');
   $this->Write(6, "($Vstup)");
   $this->Ln();
}
} 

function WriteStruct (&$tpdf, $tokr, $tods, &$tdb_conn, $tdb_RWSTDOC, $JmenoS) {
$tpdf->SetFont('ArialMT','','12');
$tpdf->MultiCell(0,6, "Struktura $JmenoS je definovan� n�sledovn�:\n", 0, 'L');
$tpdf->SetFont('Arial','I','12');
//Dynamick� sestaven� popisu struktrury adresa 
$tdb_com = "select * from $tdb_RWSTDOC where RWSTWS = 'MO3' and RWSTCN = '$JmenoS'";
$tdb_rslt=odbc_exec($tdb_conn, $tdb_com);
$tpdf->Write(6, "struct ");
$tpdf->SetFont('Arial','IB','12');
$tpdf->Write(6, $JmenoS);
$tpdf->SetFont('Arial','I','12');
$tpdf->Write(6, " {\n");
$tpdf->SetX($tokr + $tods);
$tpdf->SetLeftMargin($tokr + $tods);
while(odbc_fetch_row($tdb_rslt))
{
$tpdf->SetX($tokr + $tods);
$tpdf->SetLeftMargin($tokr + $tods);
$tpdf->SetFont('Arial','I','12');
$tpdf->Write(6, trim(odbc_result($tdb_rslt, "RWSTVT"))." ");
$tpdf->SetFont('Arial','IB','12');
$tpdf->Write(6, trim(odbc_result($tdb_rslt, "RWSTVN")));
$tpdf->SetFont('ArialMT','','8');
$tpdf->Write(6, " - ".trim(odbc_result($tdb_rslt, "RWSTDS")));
$RWSTAV = trim(odbc_result($tdb_rslt, "RWSTAV"));
if ($RWSTAV != '')
   $tpdf->Write(6, " - ".$RWSTAV);
$RWSTCS = trim(odbc_result($tdb_rslt, "RWSTCS"));
if ($RWSTCS != '')
   $tpdf->Write(6, " - ".$RWSTCS);
$tpdf->Write(6, "\n");
}
$tpdf->SetX($tokr);
$tpdf->SetLeftMargin($tokr);
$tpdf->SetFont('Arial','I','12');
$tpdf->MultiCell(0,6, "}\n\n", 0, 'L');
}



$pdf=new PDF('P','mm','A4');
$pdf->AddFont('Times','','times.php');
$pdf->AddFont('TimesBD','','timesbd.php');
$pdf->AddFont('CourierNewPSMT','','cour.php');
$pdf->AddFont('CourierNewPS-BoldMT','','courbd.php');
$pdf->AddFont('Arial-BoldMT','','arialbd.php');
$pdf->AddFont('ArialMT','','arial.php');

$pdf->SetAutoPageBreak(1, $okrb);
$pdf->left_def = $okrx;
$pdf->right_def = $okrx;
$pdf->top_def = $okrt;
$pdf->doc_name = "Webov� slu�ba InsuranceService POV + HAV (k�d MO3)";

$pdf->AddPage();

$pdf->SetFont('Arial-BoldMT','','28');
$pdf->SetTextColor(30, 60, 100);
$pdf->SetXY($okrx, $okrt);
$pdf->MultiCell(0,15, "Webov� slu�ba InsuranceService POV + HAV", 0, 'C');
$pdf->SetFont('Arial-BoldMT','','20');
$pdf->Ln();
$pdf->MultiCell(0,8, "Shrnut� zm�n oproti p�edchoz� verzi slu�by MO2 pro produkt 360", 0, 'C');
$pdf->Ln();
$pdf->Odstavec("Nov� je zaveden parametr \"Smlouva->SluzbaKod\" pro rozli�en� r�zn�ch variant SOAP slu�eb pro jeden produkt, kter� se mohou odli�ovat po�tem p�ed�van�ch �daj�.
  \"Smlouva->SluzbaKod\" se mus� plnit hodnotou MO3. 
");
$pdf->Odstavec("Nov� parametry pro p�ipoji�t�n�: 
  \"Pripojisteni->ZivelPC\"            pojistn� ��stka pro �ivel 50 000 /100 000 / 150 000 
  \"Pripojisteni->ZivelSpolNejm\"  spolu��ast nejm�n� - fixn� 5000 
  \"Pripojisteni->ZivelSpolPr\"       spolu��ast % - fixn� 5
  
D�le se m�n� cena za p�ipoji�t�n� zavazadel - kles� na 160 K�, co� ov�em nem� ��dn� vliv na zm�nu vstupn�ch parametr�.

M�n� se adresa wsdl pro testovac� i ostr� prost�ed�. Obsahuje k�d MO3.

Slu�ba MO2 pro produkt 360 z�st�v� v provozu, ALE nelze p�ipojistit �ivel bez zad�n� limitu pln�n�(pojistn� ��stky), co� umo��uje a� MO3.

K�d produktu 360 z�st�v�!

Varianta \"Dobr� �idi�\" z�st�v� beze zm�ny, ale je povolena jen pro vybran� makl��e.

P�edpokl�dan� n�b�h slu�by MO3 na provozn�m prost�ed� je od 01.04.2015. V testovac�m prost�ed� lze testovat ji� nyn� (viz. adresa wsdl n�e).

");
$pdf->Ln();
$pdf->Ln();
$pdf->SetFont('Arial-BoldMT','','20');
$pdf->MultiCell(0,8, "P�ed�v�n� dat pojistn�ch smluv POVHAV", 0, 'C');
$pdf->Ln();
$pdf->Nadpis1("1. �vod");
$pdf->Odstavec("Webov� slu�ba InsuranceService (POV + HAV) je ur�en� pro p�ed�v�n� dat smluv sdru�en�ho poji�t�n� - povinn�ho ru�en� a havarijn�ho poji�t�n� makl��em poji��ovn�. Z�rove� slou�� pro on-line kontrolu a vyhodnocen� chyb v p�ed�van�ch datech. Umo�n� proveden� okam�it� opravy na stran� makl��e. Sou�asn� tak� potvrd� p�evzet� spr�vn�ch dat poji��ovnou.\nWebov� slu�ba obsahuje funkce pro p�ed�n� �daj� nov� pojistn� smlouvy, kompletn� zm�nu a storno neotaxovan� smlouvy. D�le jsou k dispozici funkce pro ov��en� a znormov�n� v�ech p�ed�van�ch adres.");
$pdf->SetFont('ArialMT','','12');
$pdf->MultiCell(0,6, "InsuranceService je dostupn� na adrese (testovac� prost�ed�):", 0, 'J');
$pdf->SetFont('arial','I','12');
$pdf->MultiCell(0,6, "http://veris.bizdata.cz:9080/IS_SLA_MO3/service.php?class=InsuranceService&wsdl", 0, 'J');
$pdf->Ln();
$pdf->Nadpis1("2. Funkce webov� slu�by");
$pdf->Nadpis2("2.1. Funkce AdresaUIR");
$pdf->Odstavec("Funkce slou�� k vyhled�n�, ov��en� a znormov�n� adresy nahl�en� klientem ve st�tn�m ��seln�ku adres UIR.");
$pdf->Odstavec("Jako vstupn� parametr se funkci p�ed�v� pole struktur \"Adresa\", ve kter�m m��e b�t teoreticky neomezen� mno�stv� jednotliv�ch adres pro vyhled�n�. Funkce vrac� pole struktur \"OdpovedAdresa\", ve kter�m jsou jednotliv� v�sledky hled�n� p�edan�ch adres. U ka�d�ho v�sledku je p�ed�v�n status operace, kter� m��e nab�vat hodnot:");
$pdf->Cell(30, 6, "-", 0, 0, "R");
$pdf->Cell(0, 6, "\"OK\" - Adresa nalezena", 0, 1);
$pdf->Cell(30, 6, "-", 0, 0, "R");
$pdf->Cell(0, 6, "\"NF\" - Zadan�m krit�ri�m neodpov�d� ��dn� adresa v ��seln�ku UIR", 0, 1);
$pdf->Cell(30, 6, "-", 0, 0, "R");
$pdf->Cell(0, 6, "\"OV\" - P��li� mnoho v�sledk�, je t�eba z��it v�b�r - p�ed�no je prvn�ch 50", 0, 1);
$pdf->Cell(30, 6, "-", 0, 0, "R");
$pdf->Cell(0, 6, "\"OU\" - Adresa nalezena jen s �daji PS�, ulice a ��slo domu", 0, 1);
$pdf->Cell(30, 6, "-", 0, 0, "R");
$pdf->Cell(0, 6, "\"OP\" - Adresa nalezena jen s �daji PS� a ��slo domu", 0, 1);
$pdf->Cell(30, 6, "-", 0, 0, "R");
$pdf->Cell(0, 6, "\"OC\" - Adresa nalezena jen s �daji PS�, Obec a ulice", 0, 1);
$pdf->Cell(30, 6, "-", 0, 0, "R");
$pdf->Cell(0, 6, "\"ER\" - P�i zpracov�n� do�lo k chyb�", 0, 1);
$pdf->Ln();
$pdf->Odstavec("P�i vyhled�v�n� funkce postupuje tak, �e nejd��ve zkus� vyhledat adresu podle zadan�ch parametr�. Pokud nen� nic nalezeno, vyhled�v� nejd��ve s parametry PS�, obec (= ulice) a ��slo domu, d�le s parametry PS�, ulice a ��slo domu. K v�sledku je�t� p�id� nalezen� adresy dotazem s parametry PS� a ��slo domu. Pokud ani p�edchoz� kombinace vstupn�ch parametr� nevedou k ��dn�mu v�sledku, funkce vyhled�v� pouze s parametry PS�, obec a ulice.");
$pdf->Odstavec("Funkce vrac� krom� znormovan�ch n�zv� ulic, ��st� obc�, m�stsk�ch ��st� a obc� tak� jednozna�n� identifik�tor do ��seln�ku adres UIR - \"UIRid\". Po �sp�n�m znormov�n� adresy se bude p�i p�ed�v�n� pojistn� smlouvy uv�d�t tento identifik�tor na m�st� jednotliv�ch adres.");
$pdf->Definice("OdpovedAdresa[]", "AdresaUIR", "Adresa[]");

$pdf->Nadpis2("2.2. Funkce NovaPS");
$pdf->Odstavec("Pomoc� funkce NovaPS p�ed�v� makl�� �daje o nov� smlouv�. �daje jsou p�i p�ed�v�n� kontrolov�ny. Pokud jsou v�echna p�edan� data v po��dku, smlouva je p�ed�na do  IS poji��ovny a makl�� je o stavu informov�n v odpov�di. Pokud jsou n�kter� �daje chybn� uvedeny, makl�� dost�v� v odpov�di chybov� status a seznam chybn�ch parametr� s popisem chyby.");
$pdf->Odstavec("Funkci NovaPS je mo�no pou��vat ve t�ech re�imech, kter� se ur�uj� pomoc� elementu \"KodRezimu\". Pokud je zasl�na hodnota \"1\", funkce pracuje pouze v re�imu v�po�tu pojistn�ho, dle p�edan�ch �daj� vr�t� vypo�ten� pojistn�. P�i zasl�n� hodnoty \"2\" funkce nejen vr�t� spo�ten� pojistn�, ale z�rove� ov���, zda nen� klient ne��douc� kv�li nadm�rn� �kodovosti. Re�im \"0\" potom slou�� pro vlastn� p�ed�n� �daj� pojistn� smlouvy poji��ovn�.");
$pdf->Odstavec("Funkci NovaPS jsou �daje o smlouv� p�ed�v�ny ve struktu�e \"Smlouva\", odpov�� definuje struktura \"OutNS\", viz popis d�le.");
$pdf->Definice("struct OutNS", "NovaPS", "struct Smlouva");

$pdf->Nadpis2("2.3. Funkce StornoPS");
$pdf->Odstavec("Funkce \"StornoPS\" slou�� k p�ed�n� informac� pojistiteli o stornovan� smlouv�. Pro identifikaci smlouvy se funkci p�ed�v� ��slo smlouvy. D�le je p�ed�v�no datum, kdy byla smlouva stornovan�. Smlouva nesm� b�t otaxovan�.");
$pdf->Odstavec("�daje o stornovan� smlouv� jsou funkci \"StornoPS\" p�ed�v�ny ve struktu�e \"StornoIn\", odpov�� definuje struktura \"OutNS\", viz popis d�le.");
$pdf->Definice("struct OutNS", "StornoPS", "struct StornoIn");

$pdf->Nadpis2("2.4. Funkce ZmenaPS");
$pdf->Odstavec("Funkce ZmenaPS slou�� pro kompletn� zm�nu v�ech �daj� smlouvy. Podm�nky pro pou�it� funkce jsou: Smlouva ji� byla jednou p�ed�na, smlouva je neotaxovan� (tzn. nebyla na ni je�t� provedena ��dn� platba).");
$pdf->Odstavec("Funkci ZmenaPS jsou �daje o smlouv� p�ed�v�ny ve struktu�e \"Smlouva\", odpov�� definuje struktura \"OutNS\", viz popis d�le.");
$pdf->Definice("struct OutNS", "ZmenaPS", "struct Smlouva");

$pdf->Nadpis2("2.5. Funkce UkonceniPS");
$pdf->Odstavec("Informace o ukon�en�ch smlouv�ch se do syst�mu pojistitele p�ed�vaj� pomoc� funkce \"UkonceniPS\". Funkci je p�ed�v�no ��slo pojistn� smlouvy, po�ad� vozidla, datum ukon�en� smlouvy, datum poji�t�n� do, k�d d�vodu z�niku dle ��seln�ku a informace, zda byla klientem vr�cena zelen� karta.", 0, 'J');
$pdf->Odstavec("Funkce vrac� krom� statusu operace (OK, ER) a p��padn� chybov� hl�ky tak� platnost nezaplacen�ho p�edpisu od a do, ��stku nezaplacen�ho p�edpisu a aktu�ln� sumu voln�ch pen�z v bance.", 0, 'J');
$pdf->Odstavec("Funkci \"UkonceniPS\" jsou �daje o ukon�en� smlouv� p�ed�v�ny ve struktu�e \"UkonceniIn\", odpov�� definuje struktura \"UkonceniOut\", viz popis d�le.", 0, 'J');
$pdf->Definice("struct UkonceniOut", "UkonceniPS", "struct UkonceniIn");

$pdf->Nadpis2("2.6. Funkce ZmenaPolPS");
$pdf->Odstavec("Zm�ny hodnot nej�ast�ji m�n�n�ch polo�ek (SPZ, ��slo TP) se do syst�mu pojistitele p�ed�v�j� pomoc� funkce \"ZmenaPolPS\". P�ed�v� se ��slo smlouvy, po�ad� vozidla, datum zm�ny a nov� �daje SPZ nebo ��sla technick�ho pr�kazu (p��padn� oboje).");
$pdf->Odstavec("Na vstupu funkce \"ZmenaPolPS\" jsou �daje ve struktu�e \"ZmenaPolIn\", odpov�� definuje struktura \"OutNS\", viz popis d�le.");
$pdf->Definice("struct OutNS", "ZmenaPolPS", "struct ZmenaPolIn");

$pdf->Nadpis2("2.7. Funkce PlatbaPS");
$pdf->Odstavec("Pro ov��en� ��stek plateb pojistn�ho na jednotliv�ch smlouv�ch a vygenerov�n� spole�n�ho variabiln�ho symbolu pro hromadnou platbu slou�� funkce \"PlatbaPS\". Na vstupu se funkci p�ed�v� seznam jednotliv�ch plateb identifikovateln�ch ��slem smlouvy. N�sleduje ��stka lh�tn�ho pojistn�ho, provize a vlastn� ��stka k �hrad�. Funkce provede kontroly jednotliv�ch polo�ek (zda smlouva existuje, zda odpov�d� lh�tn� pojistn�, zda souhlas� rozd�l lh�tn�ho pojistn�ho a provize s vlastn� ��stkou k �hrad�). Pokud je v�e OK, vrac� variabiln� symbol pro hromadnou platbu a pro kontrolu i sumu k �hrad�.");
$pdf->Odstavec("Jednotliv� �daje plateb jsou funkci \"PlatbaPS\" p�ed�v�ny v poli struktur \"PlatbaIn\", odpov�� definuje struktura \"PlatbaOut\", viz popis d�le. Pokud dojde k chyb�, vrac� se popis chyby a ��slo chybn� smlouvy v poli struktur \"PlatbaOutChyby\".");
$pdf->Definice("struct PlatbaOut", "PlatbaPS", "Array PlatbaIn");

$pdf->Ln();
$pdf->Nadpis1("3. Pou�it� struktury");
$pdf->Nadpis2("3.1. Vstupn� struktury");

 //P�ipojen� k DB 
$mdb_conn=odbc_connect($db_name, $db_user, $db_passw);
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Smlouva');

$pdf->Odstavec("
Makler = �SUPERPOJ�      komprimovan� zkratka alfanum. n�zvu makl��e do 10 znak�
ProduktKod = �360�
SluzbaKod = �MO3�
IdentZaznamu = id dle volby makl��e (unik�tnost zvy�uje v�kon � pro nov� smlouvy),
pro kalkulace pros�m v�dy stejn� nap�. vytvo�en� z 2x opakovan�ho p�id�len�ho ��seln�ho k�du PZ
CisloSmlouvy = mus� b�t vypln�no i pro kalkulaci (!) anebo m��e b�t nevypln�no v re�imu p�enosu smlouvy,
ale mus� pak b�t zvolena volba 
");


WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Subjekt');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Adresa');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'VozidloSmlouvy');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'POV');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'HAV');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Pripojisteni');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Vybava');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'ZelenaKarta');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'SlevyPOV');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Segmentace');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'SlevyHAV');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Rodina');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Vinkulace');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Prohlidka');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'PSP');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'ZdrojovaSmlouva');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'StornoIn');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'UkonceniIn');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'ZmenaPolIn');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'PlatbaIn');



$pdf->Ln();
$pdf->Ln();
$pdf->Nadpis2("3.2. V�stupn� struktury");

WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'OdpovedAdresa');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'OutNS');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Pojistne');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'Chyby');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'UkonceniOut');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'PlatbaOut');
WriteStruct ($pdf, $okrx, $ods, $mdb_conn, $db_RWSTDOC, 'PlatbaOutChyby');

$pdf->Ln();
$pdf->Ln();
$pdf->Nadpis2("4. Simulace a testov�n� slu�eb pomoc� SOAP UI");
$pdf->Ln();
$pdf->Odstavec("SOAP UI je n�stroj, kter� lze st�hnout zdarma.
XML pro testov�n� za�leme na vy��d�n� nebo lze copy&paste u��t
n�sleduj�c� v�pis.
POZOR, je t�eba upravit nastaven�:
File->Preferences->HTTP settings->Respons compression od�krtnout!");
$pdf->Ln();
$pdf->Odstavec("Vstup (kalkulace):");
$pdf->Odstavec("
<?xml version='1.0' encoding='UTF-8'?>
<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" 
xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" 
xmlns:ns1=\"http://veris.bizdata.cz\" 
xmlns:xsd=~\"http://www.w3.org/2001/XMLSchema\" 
xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
   <SOAP-ENV:Body>
      <ns1:NovaPS>
         <Smlouva xsi:type=\"ns1:Smlouva\">
            <CisloRamcoveSmlouvy xsi:type=\"xsd:string\"/>
            <!-------------------------------------------------------->
            <!-- CisloSmlouvy mus� b�t v�dy vypln�no                -->
            <!-- pro v�echny kalkulace lze pou��vat                 -->
            <!-- jedno ��slo smlouvy                                -->
            <!-------------------------------------------------------->
            <CisloSmlouvy xsi:type=\"xsd:string\">999180018</CisloSmlouvy>
            <DatumPocatku xsi:type=\"xsd:string\">20150505</DatumPocatku>
            <DatumUzavreni xsi:type=\"xsd:string\">20150505</DatumUzavreni>            
            <HAV xsi:type=\"ns1:HAV\">
               <AllRisks xsi:type=\"xsd:string\">N</AllRisks>
               <Cena xsi:type=\"xsd:string\">N</Cena>
               <HAV xsi:type=\"xsd:string\">A</HAV>
               <PojistnaCastkaHAV xsi:type=\"xsd:int\">350000</PojistnaCastkaHAV>
               <Prohlidka xsi:type=\"ns1:Prohlidka\">
                  <CasProhlidky xsi:type=\"xsd:string\">1751</CasProhlidky>
                  <DatumProhlidky xsi:type=\"xsd:string\">20141201</DatumProhlidky>
                  <VozidloPoskozeno xsi:type=\"xsd:string\">N</VozidloPoskozeno>
               </Prohlidka>
               <SlevyHAV xsi:type=\"ns1:SlevyHAV\">
                  <AktivniZabezpeceni xsi:type=\"xsd:string\">N</AktivniZabezpeceni>
                  <Imobilizer xsi:type=\"xsd:string\">A</Imobilizer>
                  <PasivniZabezpeceni xsi:type=\"xsd:string\">N</PasivniZabezpeceni>
                  <PocetMesicu xsi:type=\"xsd:int\">50</PocetMesicu>
                  <SlevaHAV xsi:type=\"xsd:int\">0</SlevaHAV>
                  <UplatnitBonusPOV xsi:type=\"xsd:string\">A</UplatnitBonusPOV>
                  <ZabezpMech xsi:type=\"xsd:string\">N</ZabezpMech>
               </SlevyHAV>
               <Spoluucast xsi:type=\"xsd:int\">5</Spoluucast>
               <SpoluucastNejm xsi:type=\"xsd:int\">5000</SpoluucastNejm>
               <TarifniSkupinaPojHAV xsi:type=\"xsd:string\">ZT</TarifniSkupinaPojHAV>
               <VcetneDPH xsi:type=\"xsd:string\">A</VcetneDPH>
            </HAV>
            <IdentZaznamu xsi:type=\"xsd:int\">180018</IdentZaznamu>
            <KodRezimu xsi:type=\"xsd:int\">1</KodRezimu>
            <Makler xsi:type=\"xsd:string\">TEST_BD</Makler>
            <MaklerKod xsi:type=\"xsd:int\">18</MaklerKod>
            <NahrazujeSmlouvu xsi:type=\"xsd:string\"/>
            <ObchodniSleva xsi:type=\"xsd:string\">20</ObchodniSleva>
            <POV xsi:type=\"ns1:POV\">
               <POV xsi:type=\"xsd:string\">A</POV>
               <PSP xsi:type=\"ns1:PSP\">
                  <NositelPSP xsi:type=\"xsd:string\">1</NositelPSP>
                  <RozhodnaDoba xsi:type=\"xsd:int\">50</RozhodnaDoba>
               </PSP>
               <RozsahKrytiKod xsi:type=\"xsd:string\">Z</RozsahKrytiKod>
               <!-------------------------------------------------------->
               <!-- Sekce Segmentace je povinn� pro re�im kalkulace    -->
               <!-- KodRezimu = 1 - kalkulace                          -->
               <!-- KodRezimu = 2 - kalkulace+lustrace klienta         -->
               <!-------------------------------------------------------->
               <!-- KodRezimu = 0 - p�enos smlouvy                     -->
               <!-- pro KodRezimu 0 mus� b�t vypln�n pojistn�k,        -->
               <!-- provozovatel a vlastn�k                            -->
               <!-------------------------------------------------------->
               <Segmentace xsi:type=\"ns1:Segmentace\">
                  <DatNarProvozovatele xsi:type=\"xsd:int\">2031986</DatNarProvozovatele>
                  <PSCProvozovatele xsi:type=\"xsd:string\">12000</PSCProvozovatele>
                  <TypPojistnika xsi:type=\"xsd:string\">O</TypPojistnika>
                  <TypProvozovatele xsi:type=\"xsd:string\">O</TypProvozovatele>
                  <VekRidice xsi:type=\"xsd:int\">28</VekRidice>
                  <VozIC xsi:type=\"xsd:string\"/>
               </Segmentace>
               <TarifniSkupinaPojPOV xsi:type=\"xsd:string\">3B</TarifniSkupinaPojPOV>
            </POV>
            <PocetSplatek xsi:type=\"xsd:int\">1</PocetSplatek>
            <Pripojisteni xsi:type=\"ns1:Pripojisteni\">
               <Asistence xsi:type=\"xsd:string\">N</Asistence>
               <CelniSklo xsi:type=\"xsd:string\"/>
               <CelniSkloPC xsi:type=\"xsd:int\">0</CelniSkloPC>
               <CelniSkloSpolNejm xsi:type=\"xsd:int\">0</CelniSkloSpolNejm>
               <CelniSkloSpolPr xsi:type=\"xsd:int\">0</CelniSkloSpolPr>
               <PocetSedadel xsi:type=\"xsd:int\">5</PocetSedadel>
               <PoskozeniSkelCastka xsi:type=\"xsd:int\">0</PoskozeniSkelCastka>
               <PoskozeniSkelSpolMin xsi:type=\"xsd:int\">0</PoskozeniSkelSpolMin>
               <StretZver xsi:type=\"xsd:string\">N</StretZver>
               <StretZverPC xsi:type=\"xsd:int\">0</StretZverPC>
               <StretZverSpolMin xsi:type=\"xsd:int\">0</StretZverSpolMin>
               <StretZverSpolPr xsi:type=\"xsd:int\">0</StretZverSpolPr>
               <UrazOsob xsi:type=\"xsd:string\">N</UrazOsob>
               <UrazOsobNasobek xsi:type=\"xsd:int\">2</UrazOsobNasobek>
               <UrazRidice xsi:type=\"xsd:string\"/>
               <UrazRidiceNasobek xsi:type=\"xsd:int\">0</UrazRidiceNasobek>
               <VsechnaSkla xsi:type=\"xsd:string\">A</VsechnaSkla>
               <VsechnaSklaPC xsi:type=\"xsd:int\">10000</VsechnaSklaPC>
               <VsechnaSklaSpolNejm xsi:type=\"xsd:int\">500</VsechnaSklaSpolNejm>
               <VsechnaSklaSpolPr xsi:type=\"xsd:int\">0</VsechnaSklaSpolPr>
               <Vybava xsi:type=\"ns1:Vybava\"/>
               <Zavazadla xsi:type=\"xsd:string\">N</Zavazadla>
               <Zivel xsi:type=\"xsd:string\">N</Zivel>
               <ZivelPC xsi:type=\"xsd:int\">100000</ZivelPC>
               <ZivelSpolNejm xsi:type=\"xsd:int\">5000</ZivelSpolNejm>
               <ZivelSpolPr xsi:type=\"xsd:int\">5</ZivelSpolPr>
            </Pripojisteni>
            <ProduktKod xsi:type=\"xsd:string\">360</ProduktKod>
            <SluzbaKod xsi:type=\"xsd:string\">MO3</SluzbaKod>
            <VozidloSmlouvy xsi:type=\"ns1:VozidloSmlouvy\">
               <BarvaText xsi:nil=\"true\"/>
               <CelkovaHmotnost xsi:type=\"xsd:int\">1200</CelkovaHmotnost>
               <CisloTP xsi:nil=\"true\"/>
               <Datum1Registrace xsi:type=\"xsd:string\">20100101</Datum1Registrace>
               <DenDoProvozu xsi:type=\"xsd:int\">0</DenDoProvozu>
               <DruhUziti xsi:type=\"xsd:string\">0</DruhUziti>
               <KodModeluVozidla xsi:type=\"xsd:int\">94</KodModeluVozidla>
               <KodZnackyVozidla xsi:type=\"xsd:int\">135</KodZnackyVozidla>
               <Leasing xsi:type=\"xsd:string\"/>
               <MPZ xsi:type=\"xsd:string\">CS</MPZ>
               <MaxPocetOsob xsi:type=\"xsd:int\">5</MaxPocetOsob>
               <ModelText xsi:nil=\"true\"/>
               <Palivo xsi:type=\"xsd:string\">BE</Palivo>
               <PocetMistCelkem xsi:type=\"xsd:int\">5</PocetMistCelkem>
               <PoradiVozidla xsi:type=\"xsd:int\">1</PoradiVozidla>
               <RokDoProvozu xsi:type=\"xsd:int\">2010</RokDoProvozu>
               <SPZ xsi:type=\"xsd:string\">NEUVEDENA</SPZ>
               <VIN xsi:nil=\"true\"/>
               <VykonMotoru xsi:type=\"xsd:int\">83</VykonMotoru>
               <ZdvihovyObjem xsi:type=\"xsd:int\">1598</ZdvihovyObjem>
               <ZnackaText xsi:nil=\"true\"/>
            </VozidloSmlouvy>
            <ZpusobPlatby xsi:type=\"xsd:string\">B</ZpusobPlatby>
         </Smlouva>
      </ns1:NovaPS>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
");

$pdf->Odstavec("V�stup:");
$pdf->Odstavec("
<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" 
  xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" 
  xmlns:ns1=\"http://veris.bizdata.cz\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" 
  xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
   <SOAP-ENV:Body>
      <ns1:NovaPSResponse>
         <NovaPSReturn xsi:type=\"ns1:OutNS\">
            <Chyby xsi:nil=\"true\" xsi:type=\"ns1:ChybyArray\"/>
            <KontrolaKlienta xsi:type=\"xsd:string\">A</KontrolaKlienta>
            <Pojistne xsi:type=\"ns1:Pojistne\">
               <LhutniPojistne xsi:type=\"xsd:int\">15539</LhutniPojistne>
               <PojistneAsistence xsi:type=\"xsd:int\">0</PojistneAsistence>
               <PojistneCelniSklo xsi:type=\"xsd:int\">0</PojistneCelniSklo>
               <PojistneHAV xsi:type=\"xsd:int\">10819</PojistneHAV>
               <PojistneOdpovednost xsi:nil=\"true\"/>
               <PojistneOkna xsi:type=\"xsd:int\">1400</PojistneOkna>
               <PojistnePOV xsi:type=\"xsd:int\">7205</PojistnePOV>
               <PojistneUrazOsob xsi:type=\"xsd:int\">0</PojistneUrazOsob>
               <PojistneUrazRidice xsi:type=\"xsd:int\">0</PojistneUrazRidice>
               <PojistneVybava xsi:type=\"xsd:int\">0</PojistneVybava>
               <PojistneZavazadla xsi:type=\"xsd:int\">0</PojistneZavazadla>
               <PojistneZivel xsi:type=\"xsd:int\">0</PojistneZivel>
               <PredbonusemPOV xsi:type=\"xsd:int\">9007</PredbonusemPOV>
               <PredslevamiPOV xsi:type=\"xsd:int\">11547</PredslevamiPOV>
               <RocniPojistne xsi:type=\"xsd:int\">15539</RocniPojistne>
               <StretZverPojistne xsi:type=\"xsd:int\">0</StretZverPojistne>
               <VyslKoefPOV xsi:type=\"xsd:float\">1.735</VyslKoefPOV>
               <ZakladnisazbaPOV xsi:type=\"xsd:int\">5191</ZakladnisazbaPOV>
            </Pojistne>
            <RozhodnaDobaORK xsi:type=\"xsd:int\">0</RozhodnaDobaORK>
            <Status xsi:type=\"xsd:string\">OK</Status>
         </NovaPSReturn>
      </ns1:NovaPSResponse>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
");

$pdf->Output("output/WS_InsuranceServiceMO3_V3.pdf", 'F');
?>
